#include <mpi.h>
#include <strings.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>
#include <omp.h>
#include <vector>
#include <string>
#include <pthread.h>

#define USE_PAPI 1

#if USE_PAPI == 1
#include "papi.h"
#define PAPI_EVENTS 2
#endif


using namespace std;

enum commType {
	  REMOTE=0,
    LOCAL,
		BOUNDARY
};

typedef struct positionStruct {
	int x;
	int y;
	int z;
} Position;

typedef struct NeighborStruct {
	commType type;
	int globalRankId;
	int globalProcessId;
	int NodeId;
	Position localRank;
} Neighbor;

const int BLOCKZ=96;
const int BLOCKY=64;

int count;
bool localcomm;
double localError;
int N;
vector<int> MPIRankAffinity;
int px, py, pz;
int lx, ly, lz;
int nx, ny, nz;
int fx, fy, fz;
int nodeId;
int MPIRank;
int globalProcessCount;
int globalProcessId;
int localRankCount;
int globalRankCount;
int nIters;

long_long computeMissesProcess[PAPI_EVENTS];
long_long computeMissesSumProcess[PAPI_EVENTS];

double *U, *Un;
int* globalNodeId;
int* globalRankX;
int* globalRankY;
int* globalRankZ;
int*** globalProcessMapping;
int subMapCountX;
int subMapCountY;
int subMapCountZ;
int*** localRankMapping;
Position* globalProcessArray;
Position* localRankArray;
int* rankProcessArray;
int* rankLocalIdArray;
MPI_Datatype faceXX_type;
MPI_Datatype faceY_type;
MPI_Datatype faceX_type;
MPI_Datatype faceZ_type;
double localRes;
double totalRes;
double localComputeTime;
double localPackTime;
double localUnpackTime;
double localSendTime;
double localRecvTime;
double localWaitTime;
double packCount;
bool useTiming;
vector<double>* _timeLapses;
vector<int>* _timeTaskId;

void setAffinity(vector<int> cpuIds)
{
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	for (int i = 0; i < cpuIds.size(); i++) CPU_SET(cpuIds[i], &cpuset);
	if (pthread_setaffinity_np(pthread_self(), sizeof(cpu_set_t), &cpuset) != 0) printf("[WARNING] Problem assingning affinity.\n");
}

void execute();

int main(int argc, char* argv[])
{
	int __threadLevel = MPI_THREAD_MULTIPLE;
	int provided;
	MPI_Init_thread(&argc, &argv, __threadLevel, &provided);
	if (provided != __threadLevel) {printf("[MATE] MPI Init Error. Level required: %d, Level provided: %d\n", __threadLevel, provided); exit(-1);}

	MPI_Comm_rank(MPI_COMM_WORLD, &globalProcessId);
	MPI_Comm_size(MPI_COMM_WORLD, &globalProcessCount);

	localcomm = true;
  count = 1;
  localError = 0;
  nIters=100;
  N = 128;
  localRankCount = 0;
  bool isMainRank = globalProcessId == 0 ? true : false;
  useTiming = true;
  localRes = 0.0;
  totalRes = 0.0;
  packCount = 1;

  MPI_Datatype posType;
	int posLength = 3; MPI_Aint posDispl = 0; MPI_Datatype oldposType = MPI_INT;
	MPI_Type_struct(1, &posLength, &posDispl, &oldposType, &posType);
	MPI_Type_commit(&posType);

	localComputeTime = 0;
	localPackTime = 0;
	localUnpackTime = 0;
	localSendTime = 0;
	localRecvTime = 0;
	localWaitTime = 0;

	for (int i = 0; i < argc; i++) {
	if(!strcmp(argv[i], "-px")) px = atoi(argv[++i]);
	if(!strcmp(argv[i], "-py")) py = atoi(argv[++i]);
	if(!strcmp(argv[i], "-pz")) pz = atoi(argv[++i]);
	}

	int mx = px, my = py, mz = pz;

	for (int i = 0; i < argc; i++) {
	if(!strcmp(argv[i], "-lx")) lx = atoi(argv[++i]);
	if(!strcmp(argv[i], "-ly")) ly = atoi(argv[++i]);
	if(!strcmp(argv[i], "-lz")) lz = atoi(argv[++i]);
	if(!strcmp(argv[i], "-n"))  N = atoi(argv[++i]);
	if(!strcmp(argv[i], "-i"))  nIters = atoi(argv[++i]);
	if(!strcmp(argv[i], "-mx")) mx = atoi(argv[++i]);
	if(!strcmp(argv[i], "-my")) my = atoi(argv[++i]);
	if(!strcmp(argv[i], "-mz")) mz = atoi(argv[++i]);
	if(!strcmp(argv[i], "-nc")) count = 0;
	if(!strcmp(argv[i], "-np")) packCount = 0;
	if(!strcmp(argv[i], "-nl")) localcomm = false;
	if(!strcmp(argv[i], "-threads")) omp_set_num_threads(atoi(argv[++i]));
	if(!strcmp(argv[i], "-nt")) useTiming = false;
	}

	#pragma omp parallel
	{
	 localRankCount = omp_get_num_threads();
	}

	for (int i = 0; i < PAPI_EVENTS; i++)
  {computeMissesProcess[i] = 0;
	computeMissesSumProcess[i] = 0;}

	if (px * py * pz != globalProcessCount) { if (isMainRank) printf("[Error] The specified px/py/pz geometry does not match the number of MATE processes (-n %d).\n", globalProcessCount); MPI_Finalize(); return 0; }
	if (lx * ly * lz != localRankCount) { if (isMainRank) printf("[Error] The specified lx/ly/lz geometry does not match the number of local tasks (--mate-tasks %d).\n", localRankCount); MPI_Finalize(); return 0;	}

	if (px % mx != 0 || py % my != 0 || pz % mz != 0) {
		printf("mx, my, and mz must all divide the number of px, py, pz, respectively.\n");
		exit(0);
	}

	if(N % px > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by px (%d)\n", N, px); MPI_Finalize(); return 0; }
	if(N % py > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by py (%d)\n", N, py); MPI_Finalize(); return 0; }
	if(N % pz > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by pz (%d)\n", N, pz); MPI_Finalize(); return 0; }

	nx = N / px;
	ny = N / py;
	nz = N / pz;

	if(nx % lx > 0) { if (isMainRank) printf("Error: nx (%d) should be divisible by lx (%d)\n", nx, lx); MPI_Finalize(); return 0; }
	if(ny % ly > 0) { if (isMainRank) printf("Error: ny (%d) should be divisible by ly (%d)\n", ny, ly); MPI_Finalize(); return 0; }
	if(nz % lz > 0) { if (isMainRank) printf("Error: nz (%d) should be divisible by lz (%d)\n", nz, lz); MPI_Finalize(); return 0; }


	globalRankCount = globalProcessCount * localRankCount;

	_timeLapses = new vector<double>[localRankCount];
	_timeTaskId = new vector<int>[localRankCount];

	for (int i = 0; i < localRankCount; i++)
	{
		_timeLapses[i].reserve(10000);
		_timeTaskId[i].reserve(10000);
	}

	int gDepth = 2; double stencil_coeff[] = { -90.0/360, 16.0/360, -1.0/360 };

  fx = nx + 2 * gDepth;
	fy = ny + 2 * gDepth;
	fz = nz + 2 * gDepth;

	U  = (double *)calloc(sizeof(double),fx*fy*fz);
	Un = (double *)calloc(sizeof(double),fx*fy*fz);

  nodeId = -1;
	char* nodeIdString = getenv("SLURM_NODEID");
	if (nodeIdString != NULL) nodeId = atoi(nodeIdString);
	globalNodeId = (int*) calloc(sizeof(int),globalProcessCount);
	MPI_Allgather(&nodeId, 1, MPI_INT, globalNodeId, 1, MPI_INT, MPI_COMM_WORLD);

  globalRankX = (int*) calloc(sizeof(int),globalProcessCount);
  globalRankY = (int*) calloc(sizeof(int),globalProcessCount);
  globalRankZ = (int*) calloc(sizeof(int),globalProcessCount);
  globalProcessMapping = (int***) calloc(sizeof(int**),pz);
  for (int i = 0; i < pz; i++) globalProcessMapping[i] = (int**) calloc (sizeof(int*),py);
  for (int i = 0; i < pz; i++) for (int j = 0; j < py; j++) globalProcessMapping[i][j] = (int*) calloc (sizeof(int),px);

  subMapCountX = px / mx;
  subMapCountY = py / my;
  subMapCountZ = pz / mz;
  int currentRank = 0;

  for (int sz = 0; sz < subMapCountZ; sz++)
  for (int sy = 0; sy < subMapCountY; sy++)
	for (int sx = 0; sx < subMapCountX; sx++)
	for (int z = sz * mz; z < (sz+1) * mz; z++)
	for (int y = sy * my; y < (sy+1) * my; y++)
	for (int x = sx * mx; x < (sx+1) * mx; x++)
	{ globalRankZ[currentRank] = z; globalRankX[currentRank] = x; globalRankY[currentRank] = y; globalProcessMapping[z][y][x] = currentRank; currentRank++; }

	int curLocalRank = 0;
  localRankMapping = (int***) calloc (sizeof(int**) , lz);
	for (int i = 0; i < lz; i++) localRankMapping[i] = (int**) calloc (sizeof(int*) , ly);
	for (int i = 0; i < lz; i++) for (int j = 0; j < ly; j++) localRankMapping[i][j] = (int*) calloc (sizeof(int) , lx);
	for (int i = 0; i < lz; i++) for (int j = 0; j < ly; j++) for (int k = 0; k < lx; k++) localRankMapping[i][j][k] = curLocalRank++;

	rankProcessArray = (int*) calloc (sizeof(int) , globalRankCount);
	for (int rank = 0; rank < globalRankCount; rank++)	rankProcessArray[rank] = rank / localRankCount;

	rankLocalIdArray = (int*) calloc (sizeof(int) , globalRankCount);
	for (int rank = 0; rank < globalRankCount; rank++)	rankLocalIdArray[rank] = rank % localRankCount;

	localRankArray = (Position*) calloc (sizeof(Position) , globalRankCount);
  for (int rank = 0; rank < globalRankCount; rank++)
  {
   int localRankId = rankLocalIdArray[rank];

  	for(int i = 0; i < lz; i++)
  	 for (int j = 0; j < ly; j++)
  		for (int k = 0; k < lx; k++)
  		 if (localRankMapping[i][j][k] == localRankId)
  			{ localRankArray[rank].z = i;  localRankArray[rank].y = j; localRankArray[rank].x = k;}
  }


	globalProcessArray = (Position*) calloc (sizeof(Position) , globalRankCount);
  for (int rank = 0; rank < globalRankCount; rank++)
  {
  	int globalProcessId = rankProcessArray[rank];

  	globalProcessArray[rank].z = globalRankZ[globalProcessId];
  	globalProcessArray[rank].y = globalRankY[globalProcessId];
    globalProcessArray[rank].x = globalRankX[globalProcessId];
  }

  Position size;
  size.x = nx / lx;  size.y = ny / ly; size.z = nz / lz;

  MPI_Type_vector(size.y, 1, fx, MPI_DOUBLE, &faceX_type); MPI_Type_commit(&faceX_type);
  int* counts = (int*) calloc (sizeof(int) , size.z);
  MPI_Aint* displs = (MPI_Aint*) calloc (sizeof(MPI_Aint) , size.z);
  MPI_Datatype* types = (MPI_Datatype*) calloc (sizeof(MPI_Datatype) , size.z);
  for (int i = 0; i < size.z; i++) { counts[i] = 1;  displs[i] = fy*fx*sizeof(double)*i; types[i] = faceX_type; }
  MPI_Type_struct(size.z, counts, displs, types, &faceXX_type); MPI_Type_commit(&faceXX_type);
  MPI_Type_vector(size.z, size.x, fx*fy, MPI_DOUBLE, &faceY_type); MPI_Type_commit(&faceY_type);
  MPI_Type_vector(size.y, size.x, fx, MPI_DOUBLE, &faceZ_type); MPI_Type_commit(&faceZ_type);

	execute();

  int nodeCount = -1;
	char* nodeCountString = getenv("SLURM_NNODES");
	if (nodeCountString != NULL) nodeCount = atoi(nodeCountString);

	int processId = 0;
	if (globalProcessId == processId && count == 1)
	{
		FILE *threadFile;
		char fileName[256];
		sprintf(fileName, "ompProcess0-%d_%dNodes.dat", localRankCount, nodeCount);
		threadFile = fopen(fileName, "w");
		fprintf(threadFile, "%d %d\n", localRankCount, 0);

		for (int threadId = 0; threadId < localRankCount; threadId++)  fprintf(threadFile, "%d %d\n", _timeLapses[threadId].size(),0);
		for (int threadId = 0; threadId < localRankCount; threadId++)
		for (int i = 0; i < _timeLapses[threadId].size(); i++)	fprintf(threadFile, "%f %d\n", _timeLapses[threadId][i], _timeTaskId[threadId][i]);
		fclose(threadFile);
	}

  MPI_Finalize();
  return 0;
}

void execute()
{
 #pragma omp parallel
 {
  int localRankId = omp_get_thread_num();
	int myRank = globalProcessId * localRankCount + localRankId;

  const char* hostnameString = getenv("SLURM_CLUSTER_NAME");
  int nodeCoreCount = atoi(getenv("SLURM_JOB_CPUS_PER_NODE"));

	vector<int> affinity;

	// For Edison Ivy Bridge
	if (strcmp(hostnameString, "edison") == 0)
	{

		affinity.push_back(myRank%24);
//		printf("MyRank: %d - MyCore: %d\n", myRank, myRank%24);
//		affinity.push_back(myRank%24 + 24);
	}

	// For Cori Haswell/KNL
	if (strcmp(hostnameString, "cori") == 0)
	{
		if (nodeCoreCount == 64)
		{
			affinity.push_back(myRank%32);
//			printf("MyRank: %d - MyCore: %d\n", myRank, myRank%32);
		}

		if (nodeCoreCount == 272)
		{
			int coreId = myRank;
			affinity.push_back(coreId%64);
			affinity.push_back(68 + coreId%64);
			affinity.push_back(136 + coreId%64);
			affinity.push_back(204 + coreId%64);
		}
	}

	setAffinity(affinity);
	sched_yield();

	#pragma omp barrier

  Neighbor East, West, North, South, Up, Down;
  Position localRank;
	Position globalProcess;
	Position size;
  Position start;
  Position end;

	int gDepth = 2; double stencil_coeff[] = { -90.0/360, 16.0/360, -1.0/360 };


  globalProcess.z = globalProcessArray[myRank].z;
  globalProcess.y = globalProcessArray[myRank].y;
  globalProcess.x = globalProcessArray[myRank].x;

  localRank.z = localRankArray[myRank].z;
  localRank.y = localRankArray[myRank].y;
  localRank.x = localRankArray[myRank].x;

	start.x = (nx / lx) * localRank.x + gDepth;
	start.y = (ny / ly) * localRank.y + gDepth;
	start.z = (nz / lz) * localRank.z + gDepth;
	end.x = start.x + (nx / lx);
	end.y = start.y + (ny / ly);
	end.z = start.z + (nz / lz);

	West.type  = LOCAL; West.globalProcessId  = globalProcessId;
	East.type  = LOCAL; East.globalProcessId  = globalProcessId;
	North.type = LOCAL; North.globalProcessId = globalProcessId;
	South.type = LOCAL; South.globalProcessId = globalProcessId;
	Up.type    = LOCAL; Up.globalProcessId    = globalProcessId;
	Down.type  = LOCAL; Down.globalProcessId  = globalProcessId;

	West.localRank.x  = localRank.x - 1; West.localRank.y  = localRank.y;     West.localRank.z  = localRank.z;
	East.localRank.x  = localRank.x + 1; East.localRank.y  = localRank.y;     East.localRank.z  = localRank.z;
	North.localRank.x = localRank.x;     North.localRank.y = localRank.y - 1; North.localRank.z = localRank.z;
	South.localRank.x = localRank.x;     South.localRank.y = localRank.y + 1; South.localRank.z = localRank.z;
	Up.localRank.x    = localRank.x;     Up.localRank.y    = localRank.y;     Up.localRank.z    = localRank.z - 1;
	Down.localRank.x  = localRank.x;     Down.localRank.y  = localRank.y;     Down.localRank.z  = localRank.z + 1;

	if (West.localRank.x  == -1) { West.type  = REMOTE;  West.localRank.x = lx-1;  West.globalRankId  = -1; }
 	if (East.localRank.x  == lx) { East.type  = REMOTE;  East.localRank.x = 0;     East.globalRankId  = -1; }
	if (North.localRank.y == -1) { North.type = REMOTE; North.localRank.y = ly-1;  North.globalRankId = -1; }
	if (South.localRank.y == ly) { South.type = REMOTE; South.localRank.y = 0;     South.globalRankId = -1; }
	if (Up.localRank.z    == -1) { Up.type    = REMOTE;    Up.localRank.z = lz-1;  Up.globalRankId    = -1; }
	if (Down.localRank.z  == lz) { Down.type  = REMOTE;  Down.localRank.z = 0;     Down.globalRankId  = -1; }

	if (globalProcess.x == 0    && localRank.x == 0)    { West.type  = BOUNDARY; for (int i = start.y-1; i < end.y+1; i++) for (int j = start.z-1; j < end.z+1; j++) { U[j*fx*fy + i*fx] = 0;          Un[j*fx*fy + i*fx] = 0; } }
	if (globalProcess.y == 0    && localRank.y == 0)    { North.type = BOUNDARY; for (int i = start.x-1; i < end.x+1; i++) for (int j = start.z-1; j < end.z+1; j++) { U[j*fx*fy + i] = 0;                 Un[j*fx*fy + i] = 0; } }
	if (globalProcess.z == 0    && localRank.z == 0)    { Up.type    = BOUNDARY; for (int i = start.x-1; i < end.x+1; i++) for (int j = start.y-1; j < end.y+1; j++) { U[j*fx + i] = 0;                        Un[j*fx + i] = 0; } }
	if (globalProcess.x == px-1 && localRank.x == lx-1) { East.type  = BOUNDARY; for (int i = start.y-1; i < end.y+1; i++) for (int j = start.z-1; j < end.z+1; j++) { U[j*fx*fy + i*fx + nx+1] = 0;   Un[j*fx*fy + i*fx + nx+1] = 0; } }
	if (globalProcess.y == py-1 && localRank.y == ly-1) { South.type = BOUNDARY; for (int i = start.x-1; i < end.x+1; i++) for (int j = start.z-1; j < end.z+1; j++) { U[j*fx*fy + (ny+1)*fx + i] = 0; Un[j*fx*fy + (ny+1)*fx + i] = 0; } }
	if (globalProcess.z == pz-1 && localRank.z == lz-1) { Down.type  = BOUNDARY; for (int i = start.x-1; i < end.x+1; i++) for (int j = start.y-1; j < end.y+1; j++) { U[(nz+1)*fx*fy + j*fx + i] = 0; Un[(nz+1)*fx*fy + j*fx + i] = 0; } }

	if(West.type  == REMOTE) West.globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x-1];
	if(East.type  == REMOTE) East.globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x+1];
	if(North.type == REMOTE) North.globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y-1][globalProcess.x];
	if(South.type == REMOTE) South.globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y+1][globalProcess.x];
	if(Up.type    == REMOTE) Up.globalProcessId    = globalProcessMapping[globalProcess.z-1][globalProcess.y][globalProcess.x];
	if(Down.type  == REMOTE) Down.globalProcessId  = globalProcessMapping[globalProcess.z+1][globalProcess.y][globalProcess.x];

	for (int i = 0; i < globalRankCount; i++)
	{
	  if (West.globalProcessId  == rankProcessArray[i]) if (West.localRank.x  == localRankArray[i].x) if (West.localRank.y  == localRankArray[i].y) if (West.localRank.z  == localRankArray[i].z) West.globalRankId = i;
	  if (East.globalProcessId  == rankProcessArray[i]) if (East.localRank.x  == localRankArray[i].x) if (East.localRank.y  == localRankArray[i].y) if (East.localRank.z  == localRankArray[i].z) East.globalRankId = i;
	  if (North.globalProcessId == rankProcessArray[i]) if (North.localRank.x == localRankArray[i].x) if (North.localRank.y == localRankArray[i].y) if (North.localRank.z == localRankArray[i].z) North.globalRankId = i;
	  if (South.globalProcessId == rankProcessArray[i]) if (South.localRank.x == localRankArray[i].x) if (South.localRank.y == localRankArray[i].y) if (South.localRank.z == localRankArray[i].z) South.globalRankId = i;
	  if (Up.globalProcessId    == rankProcessArray[i]) if (Up.localRank.x    == localRankArray[i].x) if (Up.localRank.y    == localRankArray[i].y) if (Up.localRank.z    == localRankArray[i].z) Up.globalRankId = i;
	  if (Down.globalProcessId  == rankProcessArray[i]) if (Down.localRank.x  == localRankArray[i].x) if (Down.localRank.y  == localRankArray[i].y) if (Down.localRank.z  == localRankArray[i].z) Down.globalRankId = i;
	}

  size.x = nx / lx;  size.y = ny / ly; size.z = nz / lz;
	MPI_Request request[24*gDepth];

  double** upSendBuffer    = (double**) calloc (sizeof(double*),gDepth);
  double** downSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
  double** eastSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
  double** westSendBuffer  = (double**) calloc (sizeof(double*),gDepth);
  double** northSendBuffer = (double**) calloc (sizeof(double*),gDepth);
  double** southSendBuffer = (double**) calloc (sizeof(double*),gDepth);
  double** upRecvBuffer    = (double**) calloc (sizeof(double*),gDepth);
  double** downRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
  double** eastRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
  double** westRecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
  double** northRecvBuffer = (double**) calloc (sizeof(double*),gDepth);
  double** southRecvBuffer = (double**) calloc (sizeof(double*),gDepth);

  for (int i = 0; i < gDepth; i++)
  {
		upSendBuffer[i]    = (double*) calloc (sizeof(double),size.y*size.x);
		downSendBuffer[i]  = (double*) calloc (sizeof(double),size.y*size.x);
		eastSendBuffer[i]  = (double*) calloc (sizeof(double),size.y*size.z);
		westSendBuffer[i]  = (double*) calloc (sizeof(double),size.y*size.z);
		northSendBuffer[i] = (double*) calloc (sizeof(double),size.x*size.z);
		southSendBuffer[i] = (double*) calloc (sizeof(double),size.x*size.z);
		upRecvBuffer[i]    = (double*) calloc (sizeof(double),size.y*size.x);
		downRecvBuffer[i]  = (double*) calloc (sizeof(double),size.y*size.x);
		eastRecvBuffer[i]  = (double*) calloc (sizeof(double),size.y*size.z);
		westRecvBuffer[i]  = (double*) calloc (sizeof(double),size.y*size.z);
		northRecvBuffer[i] = (double*) calloc (sizeof(double),size.x*size.z);
		southRecvBuffer[i] = (double*) calloc (sizeof(double),size.x*size.z);
  }

	int count_down  = Down.type  == REMOTE ? count : 0;
	int count_up    = Up.type    == REMOTE ? count : 0;
	int count_east  = East.type  == REMOTE ? count : 0;
	int count_west  = West.type  == REMOTE ? count : 0;
	int count_north = North.type == REMOTE ? count : 0;
	int count_south = South.type == REMOTE ? count : 0;

  int request_count = 0;

	int upMPIRank    = rankProcessArray[Up.globalRankId];
	int downMPIRank  = rankProcessArray[Down.globalRankId];
	int southMPIRank = rankProcessArray[South.globalRankId];
	int northMPIRank = rankProcessArray[North.globalRankId];
	int eastMPIRank  = rankProcessArray[East.globalRankId];
	int westMPIRank  = rankProcessArray[West.globalRankId];

	int upSendTag     = 1*localRankCount*localRankCount + localRankCount*rankLocalIdArray[myRank]    + rankLocalIdArray[Up.globalRankId];
	int downSendTag   = 2*localRankCount*localRankCount + localRankCount*rankLocalIdArray[myRank]    + rankLocalIdArray[Down.globalRankId];
	int southSendTag  = 3*localRankCount*localRankCount + localRankCount*rankLocalIdArray[myRank]    + rankLocalIdArray[South.globalRankId];
	int northSendTag  = 4*localRankCount*localRankCount + localRankCount*rankLocalIdArray[myRank]    + rankLocalIdArray[North.globalRankId];
	int eastSendTag   = 5*localRankCount*localRankCount + localRankCount*rankLocalIdArray[myRank]    + rankLocalIdArray[East.globalRankId];
	int westSendTag   = 6*localRankCount*localRankCount + localRankCount*rankLocalIdArray[myRank]    + rankLocalIdArray[West.globalRankId];

	int upRecvTag     = 2*localRankCount*localRankCount + localRankCount*rankLocalIdArray[Up.globalRankId]    + rankLocalIdArray[myRank];
	int downRecvTag   = 1*localRankCount*localRankCount + localRankCount*rankLocalIdArray[Down.globalRankId]  + rankLocalIdArray[myRank];
	int southRecvTag  = 4*localRankCount*localRankCount + localRankCount*rankLocalIdArray[South.globalRankId] + rankLocalIdArray[myRank];
	int northRecvTag  = 3*localRankCount*localRankCount + localRankCount*rankLocalIdArray[North.globalRankId] + rankLocalIdArray[myRank];
	int eastRecvTag   = 6*localRankCount*localRankCount + localRankCount*rankLocalIdArray[East.globalRankId]  + rankLocalIdArray[myRank];
	int westRecvTag   = 5*localRankCount*localRankCount + localRankCount*rankLocalIdArray[West.globalRankId]  + rankLocalIdArray[myRank];

	for (int k = start.z-gDepth; k < end.z+gDepth; k++)
	for (int j = start.y-gDepth; j < end.y+gDepth; j++)
	for (int i = start.x-gDepth; i < end.x+gDepth; i++)
		 U[k*fy*fx + j*fx + i] = 1;

	if (West.type  == BOUNDARY) for (int i = start.y-gDepth; i < end.y+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + d] = 0;
	if (North.type == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + d*fx + i] = 0;
	if (Up.type    == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.y-gDepth; j < end.y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[d*fx*fy + j*fx + i] = 0;
	if (East.type  == BOUNDARY) for (int i = start.y-gDepth; i < end.y+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + (nx+gDepth+d)] = 0;
	if (South.type == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.z-gDepth; j < end.z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + (ny+gDepth+d)*fx + i] = 0;
	if (Down.type  == BOUNDARY) for (int i = start.x-gDepth; i < end.x+gDepth; i++) for (int j = start.y-gDepth; j < end.y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[(nz+gDepth+d)*fx*fy + j*fx + i] = 0;

	West.NodeId = globalNodeId[West.globalProcessId];
	East.NodeId = globalNodeId[East.globalProcessId];
	North.NodeId = globalNodeId[North.globalProcessId];
	South.NodeId = globalNodeId[South.globalProcessId];
	Up.NodeId = globalNodeId[Up.globalProcessId];
	Down.NodeId = globalNodeId[Down.globalProcessId];

	 if (localcomm == false)
	 {
		 if (Up.NodeId     == nodeId)  count_up    = 0;
		 if (Down.NodeId   == nodeId)  count_down  = 0;
		 if (East.NodeId   == nodeId)  count_east  = 0;
		 if (West.NodeId   == nodeId)  count_west  = 0;
		 if (North.NodeId  == nodeId)  count_north = 0;
		 if (South.NodeId  == nodeId)  count_south = 0;
	 }

	//Do the main job
  int pos = 0;
	int southTAG=1, northTAG=2, eastTAG=3, westTAG=4, downTAG=5, upTAG=6;

	#pragma omp barrier
	if (localRankId == 0) MPI_Barrier(MPI_COMM_WORLD);
  #pragma omp barrier

  double computeTime = 0;
  double packTime = 0;
  double unpackTime = 0;
  double sendTime = 0;
  double recvTime = 0;
  double waitTime = 0;
  double execTime = -MPI_Wtime();
	double t_init;
	double t_end;
	double t_initLapse;
	double t_endLapse;

		#if USE_PAPI == 1

		int events[PAPI_EVENTS] = {PAPI_L2_LDM, PAPI_L2_DCA}, ret;
		long_long values[PAPI_EVENTS];
		long_long computeMisses[PAPI_EVENTS];
		long_long computeMissesSum[PAPI_EVENTS];

		for (int i = 0; i < PAPI_EVENTS; i++) { values[i] = 0; computeMisses[i] = 0; computeMissesSum[i] = 0;  }

		if (PAPI_num_counters() < 2) {
			 fprintf(stderr, "No hardware counters here, or PAPI not supported.\n");
			 exit(1);
		}
		#endif

	#pragma omp barrier


//	printf("Task: %d - Process: %d (%d, %d, %d) - LocalTaskId: %d (%d, %d, %d) \n",
//			   myRank, globalProcessId, globalProcess.x, globalProcess.y, globalProcess.z, localRankId, localRank.x, localRank.y, localRank.z); return;

	if (useTiming) t_initLapse = MPI_Wtime();

	for (int iter=0; iter<nIters; iter++)
	 {
		if (useTiming) t_init = MPI_Wtime();
			#pragma omp barrier

	#if USE_PAPI == 1
	if ((ret = PAPI_start_counters(events, PAPI_EVENTS)) != PAPI_OK) {
		 fprintf(stderr, "PAPI failed to start counters: %s\n", PAPI_strerror(ret));
		 exit(1);
	}
	#endif

			request_count =0;
			for (int k0 = start.z; k0 < end.z; k0+=BLOCKZ) {
				int k1= k0+BLOCKZ<end.z?k0+BLOCKZ:end.z;
				for (int j0 = start.y; j0 < end.y; j0+=BLOCKY) {
					int j1= j0+BLOCKY<end.y?j0+BLOCKY:end.y;
					for (int k = k0; k < k1; k++) {
						 for (int j = j0; j < j1; j++){
								#pragma vector aligned
								#pragma ivdep
								for (int i = start.x; i < end.x; i++)
								{
									double sum = 0;
									sum += U[fx*fy*k     + fx*j         + i] * stencil_coeff[0]; // Central
									for (int d = 1; d <= gDepth; d++)
									{
										double partial_sum = 0;
										partial_sum += U[fx*fy*(k-d) + fx*j         + i]; // Up
										partial_sum += U[fx*fy*(k+d) + fx*j         + i]; // Down
										partial_sum += U[fx*fy*k     + fx*j     - d + i]; // East
										partial_sum += U[fx*fy*k     + fx*j     + d + i]; // West
										partial_sum += U[fx*fy*k     + fx*(j+d)     + i]; // North
										partial_sum += U[fx*fy*k     + fx*(j-d)     + i]; // South
										sum += partial_sum * stencil_coeff[d];
									}
									Un[fx*fy*k     + fx*j + i] = sum; // Update
								}
						 }
					}
				}
			}
			double *temp = NULL;
			temp = U;
			U = Un;
			Un = temp;

			#if USE_PAPI == 1
			if ((ret = PAPI_read_counters(values, PAPI_EVENTS)) != PAPI_OK) {
				fprintf(stderr, "PAPI failed to read counters: %s\n", PAPI_strerror(ret));
				exit(1);
			}

			for (int i = 0; i < PAPI_EVENTS; i++) { computeMisses[i] += values[i]; }

			if ((ret = PAPI_stop_counters(values, PAPI_EVENTS)) != PAPI_OK) {
				 fprintf(stderr, "PAPI failed to stop counters: %s\n", PAPI_strerror(ret));
				 exit(1);
			}
			#endif

			if (useTiming) { t_end = MPI_Wtime();	computeTime += double(t_end-t_init);	}

			if (useTiming) t_init = MPI_Wtime();

		 if(Down.type  != BOUNDARY) for (int d = 0; d < gDepth; d++)  MPI_Irecv(downRecvBuffer[d],  count_down*(size.y*size.x),  MPI_DOUBLE, downMPIRank,  downRecvTag,    MPI_COMM_WORLD, &request[request_count++]);
		 if(Up.type    != BOUNDARY) for (int d = 0; d < gDepth; d++)  MPI_Irecv(upRecvBuffer[d],    count_up*(size.y*size.x),    MPI_DOUBLE, upMPIRank,    upRecvTag,  MPI_COMM_WORLD, &request[request_count++]);
		 if(East.type  != BOUNDARY) for (int d = 0; d < gDepth; d++)  MPI_Irecv(eastRecvBuffer[d],  count_east*(size.y*size.z),  MPI_DOUBLE, eastMPIRank,  eastRecvTag,  MPI_COMM_WORLD, &request[request_count++]);
		 if(West.type  != BOUNDARY) for (int d = 0; d < gDepth; d++)  MPI_Irecv(westRecvBuffer[d],  count_west*(size.y*size.z),  MPI_DOUBLE, westMPIRank,  westRecvTag,  MPI_COMM_WORLD, &request[request_count++]);
		 if(North.type != BOUNDARY) for (int d = 0; d < gDepth; d++)  MPI_Irecv(northRecvBuffer[d], count_north*(size.x*size.z), MPI_DOUBLE, northMPIRank, northRecvTag, MPI_COMM_WORLD, &request[request_count++]);
		 if(South.type != BOUNDARY) for (int d = 0; d < gDepth; d++)  MPI_Irecv(southRecvBuffer[d], count_south*(size.x*size.z), MPI_DOUBLE, southMPIRank, southRecvTag, MPI_COMM_WORLD, &request[request_count++]);

		 if (useTiming)	 { t_end = MPI_Wtime();	 recvTime += double(t_end-t_init);	 }

		 if (useTiming) t_init = MPI_Wtime();

		 if(Down.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*(end.z-gDepth+d) + fx*start.y            + start.x     ],   packCount*count_down,  faceZ_type,  downSendBuffer[d],    sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(Up.type    != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*(start.z+d)      + fx*start.y            + start.x       ],   packCount*count_up,    faceZ_type,  upSendBuffer[d],  sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(East.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*start.y            + (end.x-gDepth+d)],   packCount*count_east,  faceXX_type, eastSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(West.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*start.y            + (start.x+d)      ],   packCount*count_west,  faceXX_type, westSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(North.type != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*(start.y+d)        + start.x       ],   packCount*count_north, faceY_type,  northSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		 if(South.type != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*(end.y-gDepth+d)   + start.x      ],   packCount*count_south, faceY_type,  southSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}

		 if (useTiming) { t_end = MPI_Wtime(); packTime += double(t_end-t_init); }

		 if (useTiming) t_init = MPI_Wtime();

		 if(Down.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) MPI_Isend(downSendBuffer[d],   count_down*(size.y*size.x),  MPI_DOUBLE,  downMPIRank,  downSendTag,  MPI_COMM_WORLD, &request[request_count++]);
		 if(Up.type    != BOUNDARY) for (int d = 0; d < gDepth; d++) MPI_Isend(upSendBuffer[d],     count_up*(size.y*size.x),    MPI_DOUBLE,  upMPIRank,    upSendTag,    MPI_COMM_WORLD, &request[request_count++]);
		 if(East.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) MPI_Isend(eastSendBuffer[d],   count_east*(size.y*size.z),  MPI_DOUBLE,  eastMPIRank,  eastSendTag,  MPI_COMM_WORLD, &request[request_count++]);
		 if(West.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) MPI_Isend(westSendBuffer[d],   count_west*(size.y*size.z),  MPI_DOUBLE,  westMPIRank,  westSendTag,  MPI_COMM_WORLD, &request[request_count++]);
		 if(North.type != BOUNDARY) for (int d = 0; d < gDepth; d++) MPI_Isend(northSendBuffer[d],  count_north*(size.x*size.z), MPI_DOUBLE,  northMPIRank, northSendTag, MPI_COMM_WORLD, &request[request_count++]);
		 if(South.type != BOUNDARY) for (int d = 0; d < gDepth; d++) MPI_Isend(southSendBuffer[d],  count_south*(size.x*size.z), MPI_DOUBLE,  southMPIRank, southSendTag, MPI_COMM_WORLD, &request[request_count++]);

		 if (useTiming)	 { t_end = MPI_Wtime();	 sendTime += double(t_end-t_init); }

		 if (useTiming)
		 {
			 t_init = MPI_Wtime();
			 t_endLapse = MPI_Wtime();
			_timeLapses[localRankId].push_back(t_endLapse-t_initLapse);
			_timeTaskId[localRankId].push_back(localRankId);
		 }

		 MPI_Waitall(request_count, request, MPI_STATUS_IGNORE);

		 if (useTiming)
		 {
			 t_end = MPI_Wtime();
			 waitTime += t_end-t_init;

			_timeLapses[localRankId].push_back(t_end-t_init);
			_timeTaskId[localRankId].push_back(-1);
			t_initLapse = t_end;
		 }

		 if (useTiming) t_init = MPI_Wtime();

		 if(Down.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Unpack(downRecvBuffer[d],  packCount*count_down*sizeof(double)*size.y*size.x, &pos,   &U[fx*fy*(end.z+d)   + fx*start.y     + start.x    ],   packCount*count_down,  faceZ_type, MPI_COMM_WORLD); pos = 0;}
		 if(Up.type    != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Unpack(upRecvBuffer[d],    packCount*count_up*sizeof(double)*size.y*size.x, &pos,       &U[fx*fy*d           + fx*start.y     + start.x    ],   packCount*count_up,    faceZ_type,  MPI_COMM_WORLD); pos = 0;}
		 if(East.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Unpack(eastRecvBuffer[d],  packCount*count_east*sizeof(double)*size.y*size.z, &pos,   &U[fx*fy*start.z     + fx*start.y     + end.x + d  ],   packCount*count_east,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		 if(West.type  != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Unpack(westRecvBuffer[d],  packCount*count_west*sizeof(double)*size.y*size.z, &pos,   &U[fx*fy*start.z     + fx*start.y     + d          ],   packCount*count_west,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		 if(North.type != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Unpack(northRecvBuffer[d], packCount*count_north*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*d           + start.x      ],   packCount*count_north, faceY_type, MPI_COMM_WORLD); pos = 0;}
		 if(South.type != BOUNDARY) for (int d = 0; d < gDepth; d++) { MPI_Unpack(southRecvBuffer[d], packCount*count_south*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*(end.y+d)   + start.x      ],   packCount*count_south, faceY_type, MPI_COMM_WORLD); pos = 0;}

		 if (useTiming)	 { t_end = MPI_Wtime();	 unpackTime += double(t_end-t_init); }
  }

	#pragma omp barrier
	if (localRankId == 0) MPI_Barrier(MPI_COMM_WORLD);
	#pragma omp barrier

	execTime += MPI_Wtime();

	#if USE_PAPI == 1
	#pragma omp critical
	{
		for (int i = 0; i < PAPI_EVENTS; i++) computeMissesProcess[i] += computeMisses[i];
	}

	if(myRank ==0)	MPI_Reduce(&computeMissesProcess, &computeMissesSumProcess, PAPI_EVENTS, MPI_LONG_LONG_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	#endif


	double res = 0;
	double err = 0;
	for (int k=start.z; k<end.z; k++)
	for (int j=start.y; j<end.y; j++)
	for (int i=start.x; i<end.x; i++)
	{ double r = U[k*fy*fx + j*fx + i];  err += r * r; }
	  MPI_Reduce (&err, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	  res = sqrt(res/((double)(N-1)*(double)(N-1)*(double)(N-1)));

	 #pragma omp critical
	  { localRes += err;
			localComputeTime += computeTime;
			localPackTime += packTime;
			localUnpackTime += unpackTime;
			localSendTime += sendTime;
			localRecvTime += recvTime;
			localWaitTime += waitTime;

	  }

	 #pragma omp barrier

	if (localRankId == 0) MPI_Reduce (&localRes, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	res = sqrt(res/((double)(N-1)*(double)(N-1)*(double)(N-1)));

  double meanComputeTime = 0, sumComputeTime = 0;
  double meanPackTime = 0, sumPackTime = 0;
  double meanUnpackTime = 0, sumUnpackTime = 0;
  double meanSendTime = 0, sumSendTime = 0;
  double meanRecvTime = 0, sumRecvTime = 0;
  double meanWaitTime = 0, sumWaitTime = 0;

  if (localRankId == 0) MPI_Reduce(&localComputeTime, &sumComputeTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (localRankId == 0) MPI_Reduce(&localPackTime, &sumPackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (localRankId == 0) MPI_Reduce(&localUnpackTime, &sumUnpackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (localRankId == 0) MPI_Reduce(&localSendTime, &sumSendTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (localRankId == 0) MPI_Reduce(&localRecvTime, &sumRecvTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  if (localRankId == 0) MPI_Reduce(&localWaitTime, &sumWaitTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  meanComputeTime = sumComputeTime / globalRankCount;
  meanPackTime = sumPackTime / globalRankCount;
  meanUnpackTime = sumUnpackTime / globalRankCount;
  meanSendTime = sumSendTime / globalRankCount;
  meanRecvTime = sumRecvTime / globalRankCount;
  meanWaitTime = sumWaitTime / globalRankCount;

	//  if (myRank == 0)
	//  {
	//   for (int z = 0; z < fz; z++)
	//   {
	//  	for (int y = 0; y < fy; y++)
	//  	{
	//  		for (int x = 0; x < fx; x++)
	//  		{
	//  			printf("%.4f ", U[z*fy*fx + y*fx + x]);
	//  		}
	//  		printf("\n");
	//  	}
	//  	printf("\n-----------------------------------\n");
	//   }
	//  }

  double totalTime = meanComputeTime + meanRecvTime + meanPackTime + meanSendTime + meanWaitTime + meanUnpackTime;

  if (useTiming == false) totalTime = execTime;

		if(myRank ==0) {
		double gflops = nIters*(double)N*(double)N*(double)N*(2 + gDepth * 8)/(1.0e9);
		  printf("%.4fs, %.3f GFlop/s (L2 Error: %g)\n", totalTime, gflops/totalTime, res);
		   printf("Compute:     %.4f\n", meanComputeTime);
		   printf("MPI_Irecv:   %.4f\n", meanRecvTime);
		   printf("MPI_Isend:   %.4f\n", meanSendTime);
		   printf("MPI_Pack:    %.4f\n", meanPackTime);
		   printf("MPI_Unpack:  %.4f\n", meanUnpackTime);
				#if USE_PAPI == 1
				printf("L2 Misses: %ld, L2 Accesses: %ld\n", computeMissesSum[0], computeMissesSum[1]);
				#endif
		   printf("MPI_Waitall: %.4f\n", meanWaitTime);
		   printf("%.4fs, %.3f GFlop/s (Error: %.4f); (L2 Error: %g)\n", totalTime, gflops/totalTime, execTime-totalTime, res);
		}
 }
}

