# Define problem size and iterations
if [ $SLURM_NNODES -eq 1   ]; then N=768; fi
if [ $SLURM_NNODES -eq 2   ]; then N=768; fi
if [ $SLURM_NNODES -eq 4   ]; then N=768; fi
if [ $SLURM_NNODES -eq 8   ]; then N=1152; fi
if [ $SLURM_NNODES -eq 16  ]; then N=2048; fi
if [ $SLURM_NNODES -eq 32  ]; then N=2560; fi
if [ $SLURM_NNODES -eq 64  ]; then N=3072; fi
if [ $SLURM_NNODES -eq 128 ]; then N=3072; fi
if [ $SLURM_NNODES -eq 256 ]; then N=3072; fi
if [ $SLURM_NNODES -eq 512 ]; then N=3072; fi

it=50 # N of Iterations 

function run {
# Obtaining parameters
vf=$2

if [[ $SLURM_CLUSTER_NAME == "edison" ]]; then
  procs=`expr $SLURM_NNODES \* 24`
else
	if [[ $SLURM_CPUS_ON_NODE == "64" ]]; then
	 procs=`expr $SLURM_NNODES \* 32`
	else
 	 procs=`expr $SLURM_NNODES \* 64`
 	fi
fi

if [[ $1 == *"PTHREADS"* ]]; then
 procs=`expr $procs \/ 4`
fi

tasks=`expr $procs \* $vf`

if [[ $1 == *"HYBRID"* ]]; then
lt=`expr $vf`
tasks=$procs
fi

# Determining blocking topology based on number of cores
if [ $tasks -eq 2097152 ]; then pz=128;  py=128;  px=128; fi
if [ $tasks -eq 1048576 ]; then pz=128;  py=128;  px=64;  fi
if [ $tasks -eq 524288  ]; then pz=128;  py=64;   px=64;  fi
if [ $tasks -eq 262144  ]; then pz=64;   py=64;   px=64;  fi
if [ $tasks -eq 131072  ]; then pz=64;   py=64;   px=32;  fi
if [ $tasks -eq 65536   ]; then pz=64;   py=32;   px=32;  fi
if [ $tasks -eq 32768   ]; then pz=32;   py=32;   px=32;  fi
if [ $tasks -eq 16384   ]; then pz=32;   py=32;   px=16;  fi
if [ $tasks -eq 8192    ]; then pz=32;   py=16;   px=16;  fi
if [ $tasks -eq 4096    ]; then pz=16;   py=16;   px=16;  fi
if [ $tasks -eq 2048    ]; then pz=16;   py=16;   px=8;   fi
if [ $tasks -eq 1024    ]; then pz=16;   py=8;    px=8;   fi
if [ $tasks -eq 512     ]; then pz=8;    py=8;    px=8;   fi
if [ $tasks -eq 256     ]; then pz=8;    py=8;    px=4;   fi
if [ $tasks -eq 128     ]; then pz=8;    py=4;    px=4;   fi
if [ $tasks -eq 64      ]; then pz=4;    py=4;    px=4;   fi
if [ $tasks -eq 32      ]; then pz=2;    py=4;    px=4;   fi
if [ $tasks -eq 16      ]; then pz=2;    py=2;    px=4;   fi

if [ $tasks -eq 196608  ]; then px=48;   py=64;   pz=64;  fi
if [ $tasks -eq 98304   ]; then px=32;   py=48;   pz=64;  fi
if [ $tasks -eq 49152   ]; then px=32;   py=32;   pz=48;  fi
if [ $tasks -eq 24576   ]; then px=24;   py=32;   pz=32;  fi
if [ $tasks -eq 12288   ]; then px=16;   py=24;   pz=32;  fi
if [ $tasks -eq 6144    ]; then px=16;   py=16;   pz=24;  fi
if [ $tasks -eq 3072    ]; then px=12;   py=16;   pz=16;  fi
if [ $tasks -eq 1536    ]; then px=8;    py=12;   pz=16;  fi
if [ $tasks -eq 768     ]; then px=8;    py=8;    pz=12;  fi
if [ $tasks -eq 384     ]; then px=6;    py=8;    pz=8;   fi
if [ $tasks -eq 192     ]; then px=4;    py=6;    pz=8;   fi
if [ $tasks -eq 96      ]; then px=4;    py=4;    pz=6;   fi
if [ $tasks -eq 48      ]; then px=3;    py=4;    pz=4;   fi
if [ $tasks -eq 24      ]; then px=2;    py=3;    pz=4;   fi
if [ $tasks -eq 12      ]; then px=2;    py=2;    pz=3;   fi
if [ $tasks -eq 6       ]; then px=1;    py=2;    pz=3;   fi
if [ $tasks -eq 3       ]; then px=1;    py=1;    pz=3;   fi

# Determine if using or MATE
exec="$1"
cf=""

if [[ $1 == *"TOUCAN"* ]]; then
 exec="$1 --mate-threads 1 --mate-tasks $tasks";  #--mate-profile
fi

if [[ $1 == *"PTHREADS"* ]]; then
 cf="-lx 1 -ly 2 -lz 2 -threads 4"
fi

if [[ $1 == *"HYBRID"* ]]; then
 cf="-lx 1 -ly 1 -lz 1"
 tasks=`expr $procs \* $vf`
 exec="$1 --mate-threads 1 --mate-tasks $tasks"; #--mate-profile
fi

if [[ $1 == *"AMPI"* ]]; then
 cf="+vp64 +pemap 0-63,68-131,136-199,204-267"; #--mate-profile
fi

runComm=srun
if [[ $1 == *"UPC"* ]]; then
 runComm="upcxx-run";
fi

command="$runComm -n $procs $exec $pars -n $N -i $it -px $px -py $py -pz $pz $cf"
#echo "[Testing] $1 - V=$vf"
echo $command
}

#echo "NOTE: OMP versions require a -n divisible by 24."
#run ./stencil_UPC 1 
run ./stencil_MPI 1 
run ./stencil_UPC 1
run ./stencil_HYBRID 1 
echo "upcxx-run -n 32 ./stencil_UPCMATE --mate-threads 1 --mate-tasks 32 -n 768 -i 50 -px 4 -py 4 -pz 2"
#run ./stencil_COARSE 1 
#run ./stencil_COARSE 2 
#run ./stencil_COARSE 4 
#run ./stencil_COARSE 8 
#run ./stencil_REFINE 1 
#run ./stencil_REFINE 2 
#run ./stencil_REFINE 4 
#run ./stencil_REFINE 8 
#run ./stencil_SHARED 1 
#run ./stencil_SHARED 2 
#run ./stencil_SHARED 4 
#run ./stencil_SHARED 8 


