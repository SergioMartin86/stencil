#include <mpi.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <string>
#include <fstream>
#include <vector>
#include <upcxx/upcxx.hpp>
#include <sched.h>

#define USE_PAPI 0
#define GHOST_DEPTH 2

#if USE_PAPI == 1
#include "papi.h"
#endif

using namespace std;

const int BLOCKZ=64;
const int BLOCKY=64;


void setAffinity(vector<int> cpuIds)
{
	cpu_set_t cpuset;
	CPU_ZERO(&cpuset);
	for (int i = 0; i < cpuIds.size(); i++) CPU_SET(cpuIds[i], &cpuset);
	if (sched_setaffinity(0, sizeof(cpu_set_t), &cpuset) != 0) printf("[WARNING] Problem assingning affinity.\n");
}

enum commType {
	  REMOTE=0,
    LOCAL,
		BOUNDARY
};

typedef struct positionStruct {
	int x;
	int y;
	int z;
} Position;

typedef struct NeighborStruct {
	commType type;
	int globalRankId;
	int globalProcessId;
	upcxx::global_ptr<double> recvBufferUPC[GHOST_DEPTH];
	int step;
} Neighbor;

typedef struct global_ptr {
    void* x;
    void* y;
} X;

Neighbor East, West, North, South, Up, Down;

int main(int carg, char* argv[])
{
	int globalProcessId = 0;
	int globalProcessCount = 1;
  int myRank, globalRankCount;

	upcxx::init();
	MPI_Init(&carg, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &globalRankCount);

  myRank = upcxx::rank_me();
  globalProcessId = myRank;
  globalProcessCount = globalRankCount;

  const char* hostnameString = getenv("SLURM_CLUSTER_NAME");
  int nodeCoreCount = atoi(getenv("SLURM_JOB_CPUS_PER_NODE"));

  vector<double> _timeLapses;
  vector<int> _timeTaskId;

	vector<int> affinity;

	// For Edison Ivy Bridge
	if (strcmp(hostnameString, "edison") == 0)
	{
		affinity.push_back(myRank%24);
		affinity.push_back(myRank%24 + 24);
	}

	// For Cori Haswell/KNL
	if (strcmp(hostnameString, "cori") == 0)
	{
		if (nodeCoreCount == 64)
		{
			affinity.push_back(myRank%32);
		}

		if (nodeCoreCount == 272)
		{
			int coreId = myRank;
			affinity.push_back(coreId%64);
			affinity.push_back(68 + coreId%64);
			affinity.push_back(136 + coreId%64);
			affinity.push_back(204 + coreId%64);
		}
	}

	setAffinity(affinity);
	sched_yield();

	bool isMainRank = globalProcessId == 0;
	double stencil_coeff[] = { -90.0/360, 16.0/360, -1.0/360 };
	// int GHOST_DEPTH = 1; double stencil_coeff[] = { -6.0/12.0, 1.0/12.0 };

	int N = 128;
	int nx, ny, nz;
	int nIters=100;
  int count = 1;
  bool localcomm = true;
  bool useTiming = true;
  double packCount = 1;
	int px=1, py=1, pz=1;
	int rankx, ranky, rankz;
	Position globalProcess;
	Position size;
  Position start;
  Position end;

	MPI_Datatype posType;
	int posLength = 3; MPI_Aint posDispl = 0; MPI_Datatype oldposType = MPI_INT;
	MPI_Type_struct(1, &posLength, &posDispl, &oldposType, &posType);
	MPI_Type_commit(&posType);

  int nMessages = 1;
  int sizeMultiplier = 1;
  int sizeDivider = 1;

  for (int i = 0; i < carg; i++) {
	if(!strcmp(argv[i], "-px")) px = atoi(argv[++i]);
	if(!strcmp(argv[i], "-py")) py = atoi(argv[++i]);
	if(!strcmp(argv[i], "-pz")) pz = atoi(argv[++i]);
	if(!strcmp(argv[i], "-n"))  N = atoi(argv[++i]);
	if(!strcmp(argv[i], "-i"))  nIters = atoi(argv[++i]);
  if(!strcmp(argv[i], "-nc")) count = 0;
  if(!strcmp(argv[i], "-np")) packCount = 0;
  if(!strcmp(argv[i], "-nl")) localcomm = false;
  if(!strcmp(argv[i], "-nt")) useTiming = false;
	if(!strcmp(argv[i], "-xm")) nMessages = atoi(argv[++i]);
	if(!strcmp(argv[i], "-xs")) sizeMultiplier = atoi(argv[++i]);
	if(!strcmp(argv[i], "-xd")) sizeDivider = atoi(argv[++i]);
	}

  int mx = px, my = py, mz = pz;

  for (int i = 0; i < carg; i++) {
  if(!strcmp(argv[i], "-mx")) mx = atoi(argv[++i]);
  if(!strcmp(argv[i], "-my")) my = atoi(argv[++i]);
  if(!strcmp(argv[i], "-mz")) mz = atoi(argv[++i]);
	}

	if (px * py * pz != globalProcessCount) { if (isMainRank) printf("[Error] The specified px/py/pz geometry does not match the number of MPI processes (-n %d).\n", globalProcessCount); MPI_Finalize(); return 0; }

	if(N % px > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by px (%d)\n", N, px); MPI_Finalize(); return 0; }
	if(N % py > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by py (%d)\n", N, py); MPI_Finalize(); return 0; }
	if(N % pz > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by pz (%d)\n", N, pz); MPI_Finalize(); return 0; }

	nx = N / px;
	ny = N / py;
	nz = N / pz;

  double *U, *Un, *b;

  int fx = nx + 2 * GHOST_DEPTH;
  int fy = ny + 2 * GHOST_DEPTH;
  int fz = nz + 2 * GHOST_DEPTH;

	U  = (double *)calloc(sizeof(double),fx*fy*fz);
	Un = (double *)calloc(sizeof(double),fx*fy*fz);

  int* globalRankX = (int*) calloc(sizeof(int),globalProcessCount);
  int* globalRankY = (int*) calloc(sizeof(int),globalProcessCount);
  int* globalRankZ = (int*) calloc(sizeof(int),globalProcessCount);
  int*** globalProcessMapping = (int***) calloc(sizeof(int**),pz);
  for (int i = 0; i < pz; i++) globalProcessMapping[i] = (int**) calloc (sizeof(int*),py);
  for (int i = 0; i < pz; i++) for (int j = 0; j < py; j++) globalProcessMapping[i][j] = (int*) calloc (sizeof(int),px);

  int subMapCountX = px / mx;
  int subMapCountY = py / my;
  int subMapCountZ = pz / mz;
  int currentRank = 0;

  for (int sz = 0; sz < subMapCountZ; sz++)
  for (int sy = 0; sy < subMapCountY; sy++)
	for (int sx = 0; sx < subMapCountX; sx++)
	for (int z = sz * mz; z < (sz+1) * mz; z++)
	for (int y = sy * my; y < (sy+1) * my; y++)
	for (int x = sx * mx; x < (sx+1) * mx; x++)
	{ globalRankZ[currentRank] = z; globalRankX[currentRank] = x; globalRankY[currentRank] = y; globalProcessMapping[z][y][x] = currentRank; currentRank++; }


//  if (globalProcessId == 0) for (int i = 0; i < globalProcessCount; i++) printf("Rank %d - Z: %d, Y: %d, X: %d\n", i, globalRankZ[i], globalRankY[i], globalRankX[i]);

	start.x = GHOST_DEPTH;
	start.y = GHOST_DEPTH;
	start.z = GHOST_DEPTH;
	end.x = start.x + nx;
	end.y = start.y + ny;
	end.z = start.z + nz;

  globalProcess.z = globalRankZ[globalProcessId];
  globalProcess.y = globalRankY[globalProcessId];
  globalProcess.x = globalRankX[globalProcessId];

	West.type  = REMOTE; West.globalProcessId  = globalProcessId;
	East.type  = REMOTE; East.globalProcessId  = globalProcessId;
	North.type = REMOTE; North.globalProcessId = globalProcessId;
	South.type = REMOTE; South.globalProcessId = globalProcessId;
	Up.type    = REMOTE; Up.globalProcessId    = globalProcessId;
	Down.type  = REMOTE; Down.globalProcessId  = globalProcessId;

	Position* globalProcessArray = (Position*) calloc (sizeof(Position) , globalRankCount);
	MPI_Allgather(&globalProcess, 1, posType, globalProcessArray, 1, posType, MPI_COMM_WORLD);

	int* rankProcessArray = (int*) calloc (sizeof(int) , globalRankCount);
	MPI_Allgather(&globalProcessId, 1, MPI_INT, rankProcessArray, 1, MPI_INT, MPI_COMM_WORLD);

	if (globalProcess.x == 0)    West.type  = BOUNDARY; West.step = 0;
	if (globalProcess.y == 0)    North.type = BOUNDARY; North.step = 0;
	if (globalProcess.z == 0)    Up.type    = BOUNDARY; Up.step = 0;
	if (globalProcess.x == px-1) East.type  = BOUNDARY; East.step = 0;
	if (globalProcess.y == py-1) South.type = BOUNDARY; South.step = 0;
	if (globalProcess.z == pz-1) Down.type  = BOUNDARY; Down.step = 0;

	if(West.type  == REMOTE) West.globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x-1];
	if(East.type  == REMOTE) East.globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x+1];
	if(North.type == REMOTE) North.globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y-1][globalProcess.x];
	if(South.type == REMOTE) South.globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y+1][globalProcess.x];
	if(Up.type    == REMOTE) Up.globalProcessId    = globalProcessMapping[globalProcess.z-1][globalProcess.y][globalProcess.x];
	if(Down.type  == REMOTE) Down.globalProcessId  = globalProcessMapping[globalProcess.z+1][globalProcess.y][globalProcess.x];

	for (int i = 0; i < globalRankCount; i++)
	{
	  if (West.globalProcessId  == rankProcessArray[i]) West.globalRankId = i;
	  if (East.globalProcessId  == rankProcessArray[i]) East.globalRankId = i;
	  if (North.globalProcessId == rankProcessArray[i]) North.globalRankId = i;
	  if (South.globalProcessId == rankProcessArray[i]) South.globalRankId = i;
	  if (Up.globalProcessId    == rankProcessArray[i]) Up.globalRankId = i;
	  if (Down.globalProcessId  == rankProcessArray[i]) Down.globalRankId = i;
	}

	free(globalProcessMapping);
	free(globalProcessArray);
	free(rankProcessArray);

  size.x = nx;  size.y = ny; size.z = nz;
	int southTAG=1, northTAG=2, eastTAG=3, westTAG=4, downTAG=5, upTAG=6;

  MPI_Datatype faceXX_type;
  MPI_Datatype faceY_type;
  MPI_Datatype faceX_type;
  MPI_Datatype faceZ_type;

  MPI_Type_vector(size.y, 1, fx, MPI_DOUBLE, &faceX_type); MPI_Type_commit(&faceX_type);
  int* counts = (int*) calloc (sizeof(int) , size.z);
  MPI_Aint* displs = (MPI_Aint*) calloc (sizeof(MPI_Aint) , size.z);
  MPI_Datatype* types = (MPI_Datatype*) calloc (sizeof(MPI_Datatype) , size.z);
  for (int i = 0; i < size.z; i++) { counts[i] = 1;  displs[i] = fy*fx*sizeof(double)*i; types[i] = faceX_type; }

  MPI_Type_struct(size.z / sizeDivider, counts, displs, types, &faceXX_type); MPI_Type_commit(&faceXX_type);
  MPI_Type_vector(size.z / sizeDivider, size.x, fx*fy, MPI_DOUBLE, &faceY_type); MPI_Type_commit(&faceY_type);
  MPI_Type_vector(size.y / sizeDivider, size.x, fx, MPI_DOUBLE, &faceZ_type); MPI_Type_commit(&faceZ_type);

	int count_down  = Down.type  == REMOTE ? count : 0;
	int count_up    = Up.type    == REMOTE ? count : 0;
	int count_east  = East.type  == REMOTE ? count : 0;
	int count_west  = West.type  == REMOTE ? count : 0;
	int count_north = North.type == REMOTE ? count : 0;
	int count_south = South.type == REMOTE ? count : 0;

  int nodeId = -1;
	char* nodeIdString = getenv("SLURM_NODEID");
	if (nodeIdString != NULL) nodeId = atoi(nodeIdString);

  int* nodeIdArray = (int*) malloc (sizeof(int) * globalRankCount);
	 MPI_Allgather(&nodeId, 1, MPI_INT, nodeIdArray, 1, MPI_INT, MPI_COMM_WORLD);

	 if (localcomm == false) for (int i = 0; i < globalRankCount; i++)
	 if (nodeId == nodeIdArray[i])
	 {
		 if (Up.globalRankId     == i)  count_up    = 0;
		 if (Down.globalRankId   == i)  count_down  = 0;
		 if (East.globalRankId   == i)  count_east  = 0;
		 if (West.globalRankId   == i)  count_west  = 0;
		 if (North.globalRankId  == i)  count_north = 0;
		 if (South.globalRankId  == i)  count_south = 0;
	 }

	for (int k = start.z-GHOST_DEPTH; k < end.z+GHOST_DEPTH; k++)
	for (int j = start.y-GHOST_DEPTH; j < end.y+GHOST_DEPTH; j++)
	for (int i = start.x-GHOST_DEPTH; i < end.x+GHOST_DEPTH; i++)
		 U[k*fy*fx + j*fx + i] = 1;


	if (West.type  == BOUNDARY) for (int i = start.y-GHOST_DEPTH; i < end.y+GHOST_DEPTH; i++) for (int j = start.z-GHOST_DEPTH; j < end.z+GHOST_DEPTH; j++) for (int d = 0; d < GHOST_DEPTH; d++) U[j*fx*fy + i*fx + d] = 0;
	if (North.type == BOUNDARY) for (int i = start.x-GHOST_DEPTH; i < end.x+GHOST_DEPTH; i++) for (int j = start.z-GHOST_DEPTH; j < end.z+GHOST_DEPTH; j++) for (int d = 0; d < GHOST_DEPTH; d++) U[j*fx*fy + d*fx + i] = 0;
	if (Up.type    == BOUNDARY) for (int i = start.x-GHOST_DEPTH; i < end.x+GHOST_DEPTH; i++) for (int j = start.y-GHOST_DEPTH; j < end.y+GHOST_DEPTH; j++) for (int d = 0; d < GHOST_DEPTH; d++) U[d*fx*fy + j*fx + i] = 0;
	if (East.type  == BOUNDARY) for (int i = start.y-GHOST_DEPTH; i < end.y+GHOST_DEPTH; i++) for (int j = start.z-GHOST_DEPTH; j < end.z+GHOST_DEPTH; j++) for (int d = 0; d < GHOST_DEPTH; d++) U[j*fx*fy + i*fx + (nx+GHOST_DEPTH+d)] = 0;
	if (South.type == BOUNDARY) for (int i = start.x-GHOST_DEPTH; i < end.x+GHOST_DEPTH; i++) for (int j = start.z-GHOST_DEPTH; j < end.z+GHOST_DEPTH; j++) for (int d = 0; d < GHOST_DEPTH; d++) U[j*fx*fy + (ny+GHOST_DEPTH+d)*fx + i] = 0;
	if (Down.type  == BOUNDARY) for (int i = start.x-GHOST_DEPTH; i < end.x+GHOST_DEPTH; i++) for (int j = start.y-GHOST_DEPTH; j < end.y+GHOST_DEPTH; j++) for (int d = 0; d < GHOST_DEPTH; d++) U[(nz+GHOST_DEPTH+d)*fx*fy + j*fx + i] = 0;

  int request_count = 0;
  int pos = 0;

  int* countFaceX = (int*) calloc (sizeof(int), nMessages);
  int* countFaceY = (int*) calloc (sizeof(int), nMessages);
  int* countFaceZ = (int*) calloc (sizeof(int), nMessages);
  int* offsetFaceX = (int*) calloc (sizeof(int), nMessages);
  int* offsetFaceY = (int*) calloc (sizeof(int), nMessages);
  int* offsetFaceZ = (int*) calloc (sizeof(int), nMessages);

  int bufferSizeX = sizeMultiplier*size.y*size.z;
  int bufferSizeY = sizeMultiplier*size.x*size.z;
  int bufferSizeZ = sizeMultiplier*size.x*size.y;

	bufferSizeX = bufferSizeX/sizeDivider;
	bufferSizeY = bufferSizeY/sizeDivider;
	bufferSizeZ = bufferSizeZ/sizeDivider;

  int msgSizeX = bufferSizeX / nMessages;
  int msgSizeY = bufferSizeY / nMessages;
  int msgSizeZ = bufferSizeZ / nMessages;

	countFaceX[0] = msgSizeX;
	countFaceY[0] = msgSizeY;
	countFaceZ[0] = msgSizeZ;

	offsetFaceX[0] = 0;
	offsetFaceY[0] = 0;
	offsetFaceZ[0] = 0;

  for (int i = 1; i < nMessages; i++)
  {
  	countFaceX[i] = msgSizeX;
  	countFaceY[i] = msgSizeY;
  	countFaceZ[i] = msgSizeZ;

  	offsetFaceX[i] = offsetFaceX[i-1] + msgSizeX;
  	offsetFaceY[i] = offsetFaceY[i-1] + msgSizeY;
  	offsetFaceZ[i] = offsetFaceZ[i-1] + msgSizeZ;
  }

	countFaceX[nMessages-1] = bufferSizeX - offsetFaceX[nMessages-1];
	countFaceY[nMessages-1] = bufferSizeY - offsetFaceY[nMessages-1];
	countFaceZ[nMessages-1] = bufferSizeZ - offsetFaceZ[nMessages-1];

	 if (myRank == 0)  printf("Message Size (%d, %d, %d)\n", msgSizeX*sizeof(double), msgSizeY*sizeof(double), msgSizeZ*sizeof(double));

  double** upSendBuffer    = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** downSendBuffer  = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** eastSendBuffer  = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** westSendBuffer  = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** northSendBuffer = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** southSendBuffer = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** upRecvBuffer    = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** downRecvBuffer  = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** eastRecvBuffer  = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** westRecvBuffer  = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** northRecvBuffer = (double**) calloc (sizeof(double*),GHOST_DEPTH);
  double** southRecvBuffer = (double**) calloc (sizeof(double*),GHOST_DEPTH);

  upcxx::global_ptr<double>* upSendBufferUPC    = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* downSendBufferUPC  = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* eastSendBufferUPC  = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* westSendBufferUPC  = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* northSendBufferUPC = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* southSendBufferUPC = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* upRecvBufferUPC    = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* downRecvBufferUPC  = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* eastRecvBufferUPC  = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* westRecvBufferUPC  = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* northRecvBufferUPC = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);
  upcxx::global_ptr<double>* southRecvBufferUPC = (upcxx::global_ptr<double>*) calloc (sizeof(upcxx::global_ptr<double>),GHOST_DEPTH);

  for (int i = 0; i < GHOST_DEPTH; i++)
  {
    upSendBufferUPC[i]    = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeZ);
    downSendBufferUPC[i]  = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeZ);
    eastSendBufferUPC[i]  = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeX);
    westSendBufferUPC[i]  = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeX);
    northSendBufferUPC[i] = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeY);
    southSendBufferUPC[i] = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeY);
    upRecvBufferUPC[i]    = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeZ);
    downRecvBufferUPC[i]  = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeZ);
    eastRecvBufferUPC[i]  = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeX);
    westRecvBufferUPC[i]  = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeX);
    northRecvBufferUPC[i] = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeY);
    southRecvBufferUPC[i] = (upcxx::global_ptr<double>) upcxx::allocate<double>(bufferSizeY);

		upSendBuffer[i]    = upSendBufferUPC[i].local();
		downSendBuffer[i]  = downSendBufferUPC[i].local();
		eastSendBuffer[i]  = eastSendBufferUPC[i].local();
		westSendBuffer[i]  = westSendBufferUPC[i].local();
		northSendBuffer[i] = northSendBufferUPC[i].local();
		southSendBuffer[i] = southSendBufferUPC[i].local();
		upRecvBuffer[i]    = upRecvBufferUPC[i].local();
		downRecvBuffer[i]  = downRecvBufferUPC[i].local();
		eastRecvBuffer[i]  = eastRecvBufferUPC[i].local();
		westRecvBuffer[i]  = westRecvBufferUPC[i].local();
		northRecvBuffer[i] = northRecvBufferUPC[i].local();
		southRecvBuffer[i] = southRecvBufferUPC[i].local();
  }

	MPI_Request request[24*GHOST_DEPTH*nMessages];

  MPI_Barrier(MPI_COMM_WORLD);
  double computeTime = 0;
  double packTime = 0;
  double unpackTime = 0;
  double rPutTime = 0;
  double syncTime = 0;
  double execTime = -MPI_Wtime();
	double t_init;
	double t_end;
	double t_initLapse;
	double t_endLapse;

	#if USE_PAPI == 1
	#define PAPI_EVENTS 2
	int events[PAPI_EVENTS] = {PAPI_L2_LDM, PAPI_L3_LDM}, ret;
	long_long values[PAPI_EVENTS];
	long_long computeMisses[PAPI_EVENTS];
	long_long computeMissesSum[PAPI_EVENTS];

	for (int i = 0; i < PAPI_EVENTS; i++) { values[i] = 0; computeMisses[i] = 0; computeMissesSum[i] = 0;  }

	if (PAPI_num_counters() < 2) {
		 fprintf(stderr, "No hardware counters here, or PAPI not supported.\n");
		 exit(1);
	}
  #endif


  MPI_Barrier(MPI_COMM_WORLD);

//  printf("Size pointer: %d, size double: %d\n", sizeof(upcxx::global_ptr<double>), sizeof(long double));

  request_count =0;

	if(Down.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Irecv(&Down.recvBufferUPC[d],  sizeof(upcxx::global_ptr<double>), MPI_BYTE, Down.globalRankId,  upTAG,    MPI_COMM_WORLD, &request[request_count++]);
	if(Up.type    == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Irecv(&Up.recvBufferUPC[d],    sizeof(upcxx::global_ptr<double>), MPI_BYTE, Up.globalRankId,    downTAG,  MPI_COMM_WORLD, &request[request_count++]);
	if(East.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Irecv(&East.recvBufferUPC[d],  sizeof(upcxx::global_ptr<double>), MPI_BYTE, East.globalRankId,  westTAG,  MPI_COMM_WORLD, &request[request_count++]);
	if(West.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Irecv(&West.recvBufferUPC[d],  sizeof(upcxx::global_ptr<double>), MPI_BYTE, West.globalRankId,  eastTAG,  MPI_COMM_WORLD, &request[request_count++]);
	if(North.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Irecv(&North.recvBufferUPC[d], sizeof(upcxx::global_ptr<double>), MPI_BYTE, North.globalRankId, southTAG, MPI_COMM_WORLD, &request[request_count++]);
	if(South.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Irecv(&South.recvBufferUPC[d], sizeof(upcxx::global_ptr<double>), MPI_BYTE, South.globalRankId, northTAG, MPI_COMM_WORLD, &request[request_count++]);

	if(Down.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Isend(&downRecvBufferUPC[d],  sizeof(upcxx::global_ptr<double>), MPI_BYTE, Down.globalRankId,  downTAG,  MPI_COMM_WORLD, &request[request_count++]);
	if(Up.type    == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Isend(&upRecvBufferUPC[d],    sizeof(upcxx::global_ptr<double>), MPI_BYTE, Up.globalRankId,    upTAG,    MPI_COMM_WORLD, &request[request_count++]);
	if(East.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Isend(&eastRecvBufferUPC[d],  sizeof(upcxx::global_ptr<double>), MPI_BYTE, East.globalRankId,  eastTAG,  MPI_COMM_WORLD, &request[request_count++]);
	if(West.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Isend(&westRecvBufferUPC[d],  sizeof(upcxx::global_ptr<double>), MPI_BYTE, West.globalRankId,  westTAG,  MPI_COMM_WORLD, &request[request_count++]);
	if(North.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Isend(&northRecvBufferUPC[d], sizeof(upcxx::global_ptr<double>), MPI_BYTE, North.globalRankId, northTAG, MPI_COMM_WORLD, &request[request_count++]);
	if(South.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++)  MPI_Isend(&southRecvBufferUPC[d], sizeof(upcxx::global_ptr<double>), MPI_BYTE, South.globalRankId, southTAG, MPI_COMM_WORLD, &request[request_count++]);

	MPI_Waitall(request_count, request, MPI_STATUS_IGNORE);

  MPI_Barrier(MPI_COMM_WORLD);

  if (useTiming) t_initLapse = MPI_Wtime();

	for (int iter=0; iter<nIters; iter++)
	 {

		t_init = MPI_Wtime();

		request_count =0;
		for (int k0 = start.z; k0 < end.z; k0+=BLOCKZ) {
			int k1= k0+BLOCKZ<end.z?k0+BLOCKZ:end.z;
			for (int j0 = start.y; j0 < end.y; j0+=BLOCKY) {
				int j1= j0+BLOCKY<end.y?j0+BLOCKY:end.y;
				for (int k = k0; k < k1; k++) {
					 for (int j = j0; j < j1; j++){
							#pragma vector aligned
							#pragma ivdep
							for (int i = start.x; i < end.x; i++)
							{
								double sum = 0;
								sum += U[fx*fy*k     + fx*j         + i] * stencil_coeff[0]; // Central
								for (int d = 1; d <= GHOST_DEPTH; d++)
								{
									double partial_sum = 0;
									partial_sum += U[fx*fy*(k-d) + fx*j         + i]; // Up
									partial_sum += U[fx*fy*(k+d) + fx*j         + i]; // Down
									partial_sum += U[fx*fy*k     + fx*j     - d + i]; // East
									partial_sum += U[fx*fy*k     + fx*j     + d + i]; // West
									partial_sum += U[fx*fy*k     + fx*(j+d)     + i]; // North
									partial_sum += U[fx*fy*k     + fx*(j-d)     + i]; // South
									sum += partial_sum * stencil_coeff[d];
								}
								Un[fx*fy*k     + fx*j + i] = sum; // Update
							}
					 }
				}
			}
		}
		double *temp = NULL;
		temp = U;
		U = Un;
		Un = temp;

		t_end = MPI_Wtime(); computeTime += t_end-t_init; t_init = MPI_Wtime();

		if(Down.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Pack(&U[fx*fy*(end.z-GHOST_DEPTH+d) + fx*start.y            + start.x              ],   packCount*count_down,  faceZ_type,  downSendBuffer[d],    sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		if(Up.type    == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Pack(&U[fx*fy*(start.z+d)      + fx*start.y            + start.x              ],   packCount*count_up,    faceZ_type,  upSendBuffer[d],  sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
		if(East.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*start.y            + (end.x-GHOST_DEPTH+d)     ],   packCount*count_east,  faceXX_type, eastSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		if(West.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*start.y            + (start.x+d)          ],   packCount*count_west,  faceXX_type, westSendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		if(North.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*(start.y+d)        + start.x              ],   packCount*count_north, faceY_type,  northSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
		if(South.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Pack(&U[fx*fy*start.z          + fx*(end.y-GHOST_DEPTH+d)   + start.x              ],   packCount*count_south, faceY_type,  southSendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}

		t_end = MPI_Wtime(); packTime += t_end-t_init; t_init = MPI_Wtime();

		upcxx::future<> futs = upcxx::make_future();

		if(Down.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) for (int i = 0; i < nMessages; i++) futs = upcxx::when_all(futs, upcxx::rput(downSendBuffer[d]  + offsetFaceZ[i],  Down.recvBufferUPC[d],  count_down*countFaceZ[i]));
		if(Up.type    == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) for (int i = 0; i < nMessages; i++) futs = upcxx::when_all(futs, upcxx::rput(upSendBuffer[d]    + offsetFaceZ[i],  Up.recvBufferUPC[d],    count_up*countFaceZ[i]));
		if(East.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) for (int i = 0; i < nMessages; i++) futs = upcxx::when_all(futs, upcxx::rput(eastSendBuffer[d]  + offsetFaceX[i],  East.recvBufferUPC[d],  count_east*countFaceX[i]));
		if(West.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) for (int i = 0; i < nMessages; i++) futs = upcxx::when_all(futs, upcxx::rput(westSendBuffer[d]  + offsetFaceX[i],  West.recvBufferUPC[d],  count_west*countFaceX[i]));
		if(North.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) for (int i = 0; i < nMessages; i++) futs = upcxx::when_all(futs, upcxx::rput(northSendBuffer[d] + offsetFaceY[i],  North.recvBufferUPC[d], count_north*countFaceY[i]));
		if(South.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) for (int i = 0; i < nMessages; i++) futs = upcxx::when_all(futs, upcxx::rput(southSendBuffer[d] + offsetFaceY[i],  South.recvBufferUPC[d], count_south*countFaceY[i]));

		futs.wait();

		t_end = MPI_Wtime(); rPutTime += t_end-t_init; t_init = MPI_Wtime();

		if(Down.type  == REMOTE) upcxx::rpc(Down.globalRankId,  [](){Up.step++;});    else Down.step++;
		if(Up.type    == REMOTE) upcxx::rpc(Up.globalRankId,    [](){Down.step++;});  else Up.step++;
		if(East.type  == REMOTE) upcxx::rpc(East.globalRankId,  [](){West.step++;});  else East.step++;
		if(West.type  == REMOTE) upcxx::rpc(West.globalRankId,  [](){East.step++;});  else West.step++;
		if(North.type == REMOTE) upcxx::rpc(North.globalRankId, [](){South.step++;}); else North.step++;
		if(South.type == REMOTE) upcxx::rpc(South.globalRankId, [](){North.step++;}); else South.step++;

		while(Down.step <= iter || Up.step <= iter || East.step <= iter || West.step <= iter || North.step <= iter || South.step <= iter) upcxx::progress();

		t_end = MPI_Wtime(); syncTime += t_end-t_init; t_init = MPI_Wtime();

		if(Down.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Unpack(downRecvBuffer[d],  packCount*count_down*sizeof(double)*size.y*size.x,  &pos, &U[fx*fy*(end.z+d) + fx*start.y   + start.x    ], packCount*count_down,  faceZ_type, MPI_COMM_WORLD); pos = 0;}
		if(Up.type    == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Unpack(upRecvBuffer[d],    packCount*count_up*sizeof(double)*size.y*size.x,    &pos, &U[fx*fy*d         + fx*start.y   + start.x    ], packCount*count_up,    faceZ_type,  MPI_COMM_WORLD); pos = 0;}
		if(East.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Unpack(eastRecvBuffer[d],  packCount*count_east*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start.z   + fx*start.y   + end.x + d  ], packCount*count_east,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		if(West.type  == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Unpack(westRecvBuffer[d],  packCount*count_west*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start.z   + fx*start.y   + d          ], packCount*count_west,  faceXX_type, MPI_COMM_WORLD); pos = 0;}
		if(North.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Unpack(northRecvBuffer[d], packCount*count_north*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*d         + start.x    ], packCount*count_north, faceY_type, MPI_COMM_WORLD); pos = 0;}
		if(South.type == REMOTE) for (int d = 0; d < GHOST_DEPTH; d++) { MPI_Unpack(southRecvBuffer[d], packCount*count_south*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start.z   + fx*(end.y+d) + start.x    ], packCount*count_south, faceY_type, MPI_COMM_WORLD); pos = 0;}

		t_end = MPI_Wtime(); unpackTime += t_end-t_init; t_init = MPI_Wtime();
  }

	upcxx::barrier();
	execTime += MPI_Wtime();

  double res = 0;
  double err = 0;
  for (int k=start.z; k<end.z; k++)
	for (int j=start.y; j<end.y; j++)
	for (int i=start.x; i<end.x; i++)
  { double r = U[k*fy*fx + j*fx + i];  err += r * r; }
  MPI_Reduce (&err, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  res = sqrt(res/((double)(N-1)*(double)(N-1)*(double)(N-1)));

  double meanComputeTime = 0, sumComputeTime = 0;
  double meanPackTime = 0, sumPackTime = 0;
  double meanUnpackTime = 0, sumUnpackTime = 0;
  double meanRputTime = 0, sumRPutTime = 0;
  double meanSyncTime = 0, sumSyncTime = 0;

  MPI_Reduce(&computeTime, &sumComputeTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&packTime, &sumPackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&unpackTime, &sumUnpackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&rPutTime, &sumRPutTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&syncTime, &sumSyncTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  #if USE_PAPI == 1
  MPI_Reduce(&computeMisses, &computeMissesSum, PAPI_EVENTS, MPI_LONG_LONG_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  #endif

  meanComputeTime = sumComputeTime / globalRankCount;
  meanPackTime = sumPackTime / globalRankCount;
  meanUnpackTime = sumUnpackTime / globalRankCount;
  meanRputTime = sumRPutTime / globalRankCount;
  meanSyncTime = sumSyncTime / globalRankCount;

  double totalTime = meanComputeTime + meanPackTime + meanRputTime + meanSyncTime + meanUnpackTime;

  if (useTiming == false) totalTime = execTime;

  int nodeCount = -1;
	char* nodeCountString = getenv("SLURM_NNODES");
	if (nodeCountString != NULL) nodeCount = atoi(nodeCountString);


	if (myRank < 8)
	{
		FILE *threadFile;
		char fileName[256];
		sprintf(fileName, "mpiProcess_%dNodes_%d.dat", nodeCount, myRank);
		threadFile = fopen(fileName, "w");
		fprintf(threadFile, "%d %d\n", 1, 0);

		fprintf(threadFile, "%d %d\n", _timeLapses.size(),0);
		for (int i = 0; i < _timeLapses.size(); i++)	fprintf(threadFile, "%f %d\n", _timeLapses[i], _timeTaskId[i]);
		fclose(threadFile);
	}

	if(myRank ==0) {
	   double gflops = nIters*(double)N*(double)N*(double)N*(2 + GHOST_DEPTH * 8)/(1.0e9);
	   printf("Compute:  %.4f\n", meanComputeTime);
	   printf("sync:     %.4f\n", meanSyncTime);
	   printf("rput:     %.4f\n", meanRputTime);
	   printf("Pack:     %.4f\n", meanPackTime);
	   printf("Unpack:   %.4f\n", meanUnpackTime);
     #if USE_PAPI == 1
		 printf("L2 Misses: %ld, L3 Misses: %ld\n", computeMissesSum[0], computeMissesSum[1]);
	   #endif
	   printf("%.4fs, %.3f GFlop/s (Error: %.4f); (L2 Error: %g)\n", execTime, gflops/execTime, execTime-totalTime, res);
	}

    MPI_Finalize(); return 0;
}
