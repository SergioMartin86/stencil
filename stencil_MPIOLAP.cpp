#include <mpi.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <string>
#include <fstream>
#include <vector>

using namespace std;


const int BLOCKZ=96;
const int BLOCKY=64;

enum commType {
	  REMOTE=0,
    LOCAL,
		BOUNDARY
};

typedef struct positionStruct {
	int x;
	int y;
	int z;
} Position;

typedef struct NeighborStruct {
	commType type;
	int globalProcessId;
	Position localRank;
	int tag;
	double** SendBuffer;
	double ** RecvBuffer;
} Neighbor;


int main(int carg, char* argv[])
{
	int globalProcessId = 0;
	int globalProcessCount = 1;
  int myRank, globalRankCount;

	MPI_Init(&carg, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
  MPI_Comm_size(MPI_COMM_WORLD, &globalRankCount);
  globalProcessId = myRank;
  globalProcessCount = globalRankCount;

  #ifdef __MATE_RUNTIME
	Mate_local_rank_id(&localRankId);
	Mate_local_rank_count(&localRankCount);
	Mate_global_process_id(&globalProcessId);
	Mate_global_process_count(&globalProcessCount);
	Mate_local_thread_count(&localThreadCount);
	Mate_global_thread_count(&globalThreadCount);
  #endif

	bool isMainRank = globalProcessId == 0;
	int gDepth = 2; double stencil_coeff[] = { -90.0/360, 16.0/360, -1.0/360 };
	// int gDepth = 1; double stencil_coeff[] = { -6.0/12.0, 1.0/12.0 };

	int N = 128;
	int nx, ny, nz;
	int nIters=100;
  int count = 1;
	int px=1, py=1, pz=1, lx=1, ly=1, lz=1;
  bool localcomm = true;
  bool useTiming = true;
  double packCount = 1;
	int rankx, ranky, rankz;
  Neighbor *East, *West, *North, *South, *Up, *Down;
  Position* localRank;
	Position globalProcess;
	Position size;
  Position* start;
  Position* end;

	MPI_Datatype posType;
	int posLength = 3; MPI_Aint posDispl = 0; MPI_Datatype oldposType = MPI_INT;
	MPI_Type_struct(1, &posLength, &posDispl, &oldposType, &posType);
	MPI_Type_commit(&posType);

  int nMessages = 1;
  int sizeMultiplier = 1;
  int sizeDivider = 1;

  for (int i = 0; i < carg; i++) {
	if(!strcmp(argv[i], "-px")) px = atoi(argv[++i]);
	if(!strcmp(argv[i], "-py")) py = atoi(argv[++i]);
	if(!strcmp(argv[i], "-pz")) pz = atoi(argv[++i]);
	if(!strcmp(argv[i], "-lx")) lx = atoi(argv[++i]);
	if(!strcmp(argv[i], "-ly")) ly = atoi(argv[++i]);
	if(!strcmp(argv[i], "-lz")) lz = atoi(argv[++i]);
	if(!strcmp(argv[i], "-n"))  N  = atoi(argv[++i]);
	if(!strcmp(argv[i], "-i"))  nIters = atoi(argv[++i]);
  if(!strcmp(argv[i], "-nc")) count = 0;
  if(!strcmp(argv[i], "-np")) packCount = 0;
  if(!strcmp(argv[i], "-nl")) localcomm = false;
  if(!strcmp(argv[i], "-nt")) useTiming = false;
	if(!strcmp(argv[i], "-xm")) nMessages = atoi(argv[++i]);
	if(!strcmp(argv[i], "-xs")) sizeMultiplier = atoi(argv[++i]);
	if(!strcmp(argv[i], "-xd")) sizeDivider = atoi(argv[++i]);
	}

  int mx = px, my = py, mz = pz;

  for (int i = 0; i < carg; i++) {
  if(!strcmp(argv[i], "-mx")) mx = atoi(argv[++i]);
  if(!strcmp(argv[i], "-my")) my = atoi(argv[++i]);
  if(!strcmp(argv[i], "-mz")) mz = atoi(argv[++i]);
	}

	int localRankCount = lx * ly * lz;

	if (px * py * pz != globalProcessCount) { if (isMainRank) printf("[Error] The specified px/py/pz geometry does not match the number of MATE processes (-n %d).\n", globalProcessCount); MPI_Finalize(); return 0; }
	if (lx * ly * lz != localRankCount) { if (isMainRank) printf("[Error] The specified lx/ly/lz geometry does not match the number of local tasks (--mate-tasks %d).\n", localRankCount); MPI_Finalize(); return 0;	}

	if(N % px > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by px (%d)\n", N, px); MPI_Finalize(); return 0; }
	if(N % py > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by py (%d)\n", N, py); MPI_Finalize(); return 0; }
	if(N % pz > 0) { if (isMainRank) printf("Error: N (%d) should be divisible by pz (%d)\n", N, pz); MPI_Finalize(); return 0; }

	nx = N / px;
	ny = N / py;
	nz = N / pz;

	if(nx % lx > 0) { if (isMainRank) printf("Error: nx (%d) should be divisible by lx (%d)\n", nx, lx); MPI_Finalize(); return 0; }
	if(ny % ly > 0) { if (isMainRank) printf("Error: ny (%d) should be divisible by ly (%d)\n", ny, ly); MPI_Finalize(); return 0; }
	if(nz % lz > 0) { if (isMainRank) printf("Error: nz (%d) should be divisible by lz (%d)\n", nz, lz); MPI_Finalize(); return 0; }

  double *U, *Un, *b;

  int fx = nx + 2 * gDepth;
  int fy = ny + 2 * gDepth;
  int fz = nz + 2 * gDepth;

  U  = (double *)calloc(sizeof(double),fx*fy*fz);
  Un = (double *)calloc(sizeof(double),fx*fy*fz);

  int* globalRankX = (int*) calloc(sizeof(int),globalProcessCount);
  int* globalRankY = (int*) calloc(sizeof(int),globalProcessCount);
  int* globalRankZ = (int*) calloc(sizeof(int),globalProcessCount);
  int*** globalProcessMapping = (int***) calloc(sizeof(int**),pz);
  for (int i = 0; i < pz; i++) globalProcessMapping[i] = (int**) calloc (sizeof(int*),py);
  for (int i = 0; i < pz; i++) for (int j = 0; j < py; j++) globalProcessMapping[i][j] = (int*) calloc (sizeof(int),px);

  int subMapCountX = px / mx;
  int subMapCountY = py / my;
  int subMapCountZ = pz / mz;
  int currentRank = 0;

  for (int sz = 0; sz < subMapCountZ; sz++)
  for (int sy = 0; sy < subMapCountY; sy++)
	for (int sx = 0; sx < subMapCountX; sx++)
	for (int z = sz * mz; z < (sz+1) * mz; z++)
	for (int y = sy * my; y < (sy+1) * my; y++)
	for (int x = sx * mx; x < (sx+1) * mx; x++)
	{ globalRankZ[currentRank] = z; globalRankX[currentRank] = x; globalRankY[currentRank] = y; globalProcessMapping[z][y][x] = currentRank; currentRank++; }

//  if (globalProcessId == 0) for (int i = 0; i < globalProcessCount; i++) printf("Rank %d - Z: %d, Y: %d, X: %d\n", i, globalRankZ[i], globalRankY[i], globalRankX[i]);

	int curLocalRank = 0;
	int*** localRankMapping = (int***) calloc (sizeof(int**) , lz);
	for (int i = 0; i < lz; i++) localRankMapping[i] = (int**) calloc (sizeof(int*) , ly);
	for (int i = 0; i < lz; i++) for (int j = 0; j < ly; j++) localRankMapping[i][j] = (int*) calloc (sizeof(int) , lx);
	for (int i = 0; i < lz; i++) for (int j = 0; j < ly; j++) for (int k = 0; k < lx; k++) localRankMapping[i][j][k] = curLocalRank++;

	localRank = (Position*) calloc (sizeof(Position), localRankCount);
	start = (Position*) calloc (sizeof(Position), localRankCount);
	end = (Position*) calloc (sizeof(Position), localRankCount);

  globalProcess.z = globalRankZ[globalProcessId];
  globalProcess.y = globalRankY[globalProcessId];
  globalProcess.x = globalRankX[globalProcessId];

  West = (Neighbor*) calloc (sizeof(Neighbor), localRankCount);
  East = (Neighbor*) calloc (sizeof(Neighbor), localRankCount);
  North = (Neighbor*) calloc (sizeof(Neighbor), localRankCount);
  South = (Neighbor*) calloc (sizeof(Neighbor), localRankCount);
  Up = (Neighbor*) calloc (sizeof(Neighbor), localRankCount);
  Down = (Neighbor*) calloc (sizeof(Neighbor), localRankCount);

	int *count_down  = (int*) calloc (sizeof(int), localRankCount);
	int *count_up    = (int*) calloc (sizeof(int), localRankCount);
	int *count_east  = (int*) calloc (sizeof(int), localRankCount);
	int *count_west  = (int*) calloc (sizeof(int), localRankCount);
	int *count_north = (int*) calloc (sizeof(int), localRankCount);
	int *count_south = (int*) calloc (sizeof(int), localRankCount);

  size.x = nx / lx;  size.y = ny / ly; size.z = nz / lz;

  MPI_Datatype faceXX_type;
  MPI_Datatype faceY_type;
  MPI_Datatype faceX_type;
  MPI_Datatype faceZ_type;

  MPI_Type_vector(size.y, 1, fx, MPI_DOUBLE, &faceX_type); MPI_Type_commit(&faceX_type);
  int* counts = (int*) calloc (sizeof(int) , size.z);
  MPI_Aint* displs = (MPI_Aint*) calloc (sizeof(MPI_Aint) , size.z);
  MPI_Datatype* types = (MPI_Datatype*) calloc (sizeof(MPI_Datatype) , size.z);
  for (int i = 0; i < size.z; i++) { counts[i] = 1;  displs[i] = fy*fx*sizeof(double)*i; types[i] = faceX_type; }
  MPI_Type_struct(size.z / sizeDivider, counts, displs, types, &faceXX_type); MPI_Type_commit(&faceXX_type);
  MPI_Type_vector(size.z / sizeDivider, size.x, fx*fy, MPI_DOUBLE, &faceY_type); MPI_Type_commit(&faceY_type);
  MPI_Type_vector(size.y / sizeDivider, size.x, fx, MPI_DOUBLE, &faceZ_type); MPI_Type_commit(&faceZ_type);

	for (int localRankId = 0; localRankId < localRankCount; localRankId++)
	{

		for(int i = 0; i < lz; i++)
		 for (int j = 0; j < ly; j++)
			for (int k = 0; k < lx; k++)
			 if (localRankMapping[i][j][k] == localRankId)
				{ localRank[localRankId].z = i;  localRank[localRankId].y = j; localRank[localRankId].x = k;}

		start[localRankId].x = (nx / lx) * localRank[localRankId].x + gDepth;
		start[localRankId].y = (ny / ly) * localRank[localRankId].y + gDepth;
		start[localRankId].z = (nz / lz) * localRank[localRankId].z + gDepth;
		end[localRankId].x = start[localRankId].x + (nx / lx);
		end[localRankId].y = start[localRankId].y + (ny / ly);
		end[localRankId].z = start[localRankId].z + (nz / lz);

		West[localRankId].type  = LOCAL; West[localRankId].globalProcessId  = globalProcessId;
		East[localRankId].type  = LOCAL; East[localRankId].globalProcessId  = globalProcessId;
		North[localRankId].type = LOCAL; North[localRankId].globalProcessId = globalProcessId;
		South[localRankId].type = LOCAL; South[localRankId].globalProcessId = globalProcessId;
		Up[localRankId].type    = LOCAL; Up[localRankId].globalProcessId    = globalProcessId;
		Down[localRankId].type  = LOCAL; Down[localRankId].globalProcessId  = globalProcessId;

		Position* localRankArray = (Position*) calloc (sizeof(Position) , globalRankCount);
		MPI_Allgather(&localRank, 1, posType, localRankArray, 1, posType, MPI_COMM_WORLD);

		Position* globalProcessArray = (Position*) calloc (sizeof(Position) , globalRankCount);
		MPI_Allgather(&globalProcess, 1, posType, globalProcessArray, 1, posType, MPI_COMM_WORLD);

		int* rankProcessArray = (int*) calloc (sizeof(int) , globalRankCount);
		MPI_Allgather(&globalProcessId, 1, MPI_INT, rankProcessArray, 1, MPI_INT, MPI_COMM_WORLD);

		West[localRankId].localRank.x  = localRank[localRankId].x - 1; West[localRankId].localRank.y  = localRank[localRankId].y;     West[localRankId].localRank.z  = localRank[localRankId].z;
		East[localRankId].localRank.x  = localRank[localRankId].x + 1; East[localRankId].localRank.y  = localRank[localRankId].y;     East[localRankId].localRank.z  = localRank[localRankId].z;
		North[localRankId].localRank.x = localRank[localRankId].x;     North[localRankId].localRank.y = localRank[localRankId].y - 1; North[localRankId].localRank.z = localRank[localRankId].z;
		South[localRankId].localRank.x = localRank[localRankId].x;     South[localRankId].localRank.y = localRank[localRankId].y + 1; South[localRankId].localRank.z = localRank[localRankId].z;
		Up[localRankId].localRank.x    = localRank[localRankId].x;     Up[localRankId].localRank.y    = localRank[localRankId].y;     Up[localRankId].localRank.z    = localRank[localRankId].z - 1;
		Down[localRankId].localRank.x  = localRank[localRankId].x;     Down[localRankId].localRank.y  = localRank[localRankId].y;     Down[localRankId].localRank.z  = localRank[localRankId].z + 1;

		if (West[localRankId].localRank.x  == -1) { West[localRankId].type  = REMOTE;  West[localRankId].localRank.x = lx-1; }
		if (East[localRankId].localRank.x  == lx) { East[localRankId].type  = REMOTE;  East[localRankId].localRank.x = 0;    }
		if (North[localRankId].localRank.y == -1) { North[localRankId].type = REMOTE; North[localRankId].localRank.y = ly-1; }
		if (South[localRankId].localRank.y == ly) { South[localRankId].type = REMOTE; South[localRankId].localRank.y = 0;    }
		if (Up[localRankId].localRank.z    == -1) { Up[localRankId].type    = REMOTE;    Up[localRankId].localRank.z = lz-1; }
		if (Down[localRankId].localRank.z  == lz) { Down[localRankId].type  = REMOTE;  Down[localRankId].localRank.z = 0;    }

		if (globalProcess.x == 0    && localRank[localRankId].x == 0)    West[localRankId].type  = BOUNDARY;
		if (globalProcess.y == 0    && localRank[localRankId].y == 0)    North[localRankId].type = BOUNDARY;
		if (globalProcess.z == 0    && localRank[localRankId].z == 0)    Up[localRankId].type    = BOUNDARY;
		if (globalProcess.x == px-1 && localRank[localRankId].x == lx-1) East[localRankId].type  = BOUNDARY;
		if (globalProcess.y == py-1 && localRank[localRankId].y == ly-1) South[localRankId].type = BOUNDARY;
		if (globalProcess.z == pz-1 && localRank[localRankId].z == lz-1) Down[localRankId].type  = BOUNDARY;

		if(West[localRankId].type  == REMOTE) West[localRankId].globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x-1];
		if(East[localRankId].type  == REMOTE) East[localRankId].globalProcessId  = globalProcessMapping[globalProcess.z][globalProcess.y][globalProcess.x+1];
		if(North[localRankId].type == REMOTE) North[localRankId].globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y-1][globalProcess.x];
		if(South[localRankId].type == REMOTE) South[localRankId].globalProcessId = globalProcessMapping[globalProcess.z][globalProcess.y+1][globalProcess.x];
		if(Up[localRankId].type    == REMOTE) Up[localRankId].globalProcessId    = globalProcessMapping[globalProcess.z-1][globalProcess.y][globalProcess.x];
		if(Down[localRankId].type  == REMOTE) Down[localRankId].globalProcessId  = globalProcessMapping[globalProcess.z+1][globalProcess.y][globalProcess.x];

		South[localRankId].tag=localRank[localRankId].x + lx*South[localRankId].localRank.z;
		North[localRankId].tag=localRank[localRankId].x + lx*South[localRankId].localRank.z;
		East[localRankId].tag=localRank[localRankId].y + ly*South[localRankId].localRank.z;
		West[localRankId].tag=localRank[localRankId].y + ly*South[localRankId].localRank.z;
		Down[localRankId].tag=localRank[localRankId].x + lx*South[localRankId].localRank.y;
		Up[localRankId].tag=localRank[localRankId].x + lx*South[localRankId].localRank.y;

		count_down[localRankId]  = Down[localRankId].type  == REMOTE ? count : 0;
		count_up[localRankId]    = Up[localRankId].type    == REMOTE ? count : 0;
		count_east[localRankId]  = East[localRankId].type  == REMOTE ? count : 0;
		count_west[localRankId]  = West[localRankId].type  == REMOTE ? count : 0;
		count_north[localRankId] = North[localRankId].type == REMOTE ? count : 0;
		count_south[localRankId] = South[localRankId].type == REMOTE ? count : 0;

		for (int k = start[localRankId].z-gDepth; k < end[localRankId].z+gDepth; k++)
		for (int j = start[localRankId].y-gDepth; j < end[localRankId].y+gDepth; j++)
		for (int i = start[localRankId].x-gDepth; i < end[localRankId].x+gDepth; i++)
			 U[k*fy*fx + j*fx + i] = 1;

		if (West[localRankId].type  == BOUNDARY) for (int i = start[localRankId].y-gDepth; i < end[localRankId].y+gDepth; i++) for (int j = start[localRankId].z-gDepth; j < end[localRankId].z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + d] = 0;
		if (North[localRankId].type == BOUNDARY) for (int i = start[localRankId].x-gDepth; i < end[localRankId].x+gDepth; i++) for (int j = start[localRankId].z-gDepth; j < end[localRankId].z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + d*fx + i] = 0;
		if (Up[localRankId].type    == BOUNDARY) for (int i = start[localRankId].x-gDepth; i < end[localRankId].x+gDepth; i++) for (int j = start[localRankId].y-gDepth; j < end[localRankId].y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[d*fx*fy + j*fx + i] = 0;
		if (East[localRankId].type  == BOUNDARY) for (int i = start[localRankId].y-gDepth; i < end[localRankId].y+gDepth; i++) for (int j = start[localRankId].z-gDepth; j < end[localRankId].z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + i*fx + (nx+gDepth+d)] = 0;
		if (South[localRankId].type == BOUNDARY) for (int i = start[localRankId].x-gDepth; i < end[localRankId].x+gDepth; i++) for (int j = start[localRankId].z-gDepth; j < end[localRankId].z+gDepth; j++) for (int d = 0; d < gDepth; d++) U[j*fx*fy + (ny+gDepth+d)*fx + i] = 0;
		if (Down[localRankId].type  == BOUNDARY) for (int i = start[localRankId].x-gDepth; i < end[localRankId].x+gDepth; i++) for (int j = start[localRankId].y-gDepth; j < end[localRankId].y+gDepth; j++) for (int d = 0; d < gDepth; d++) U[(nz+gDepth+d)*fx*fy + j*fx + i] = 0;


	  free(localRankArray);
		free(globalProcessArray);
		free(rankProcessArray);
  }


  int* countFaceX = (int*) calloc (sizeof(int), nMessages);
	int* countFaceY = (int*) calloc (sizeof(int), nMessages);
	int* countFaceZ = (int*) calloc (sizeof(int), nMessages);
	int* offsetFaceX = (int*) calloc (sizeof(int), nMessages);
	int* offsetFaceY = (int*) calloc (sizeof(int), nMessages);
	int* offsetFaceZ = (int*) calloc (sizeof(int), nMessages);

	int bufferSizeX = sizeMultiplier*size.y*size.z;
	int bufferSizeY = sizeMultiplier*size.x*size.z;
	int bufferSizeZ = sizeMultiplier*size.x*size.y;

	bufferSizeX = bufferSizeX / sizeDivider;
	bufferSizeY = bufferSizeY / sizeDivider;
	bufferSizeZ = bufferSizeZ / sizeDivider;

	int msgSizeX = bufferSizeX / nMessages;
	int msgSizeY = bufferSizeY / nMessages;
	int msgSizeZ = bufferSizeZ / nMessages;

	countFaceX[0] = msgSizeX;
	countFaceY[0] = msgSizeY;
	countFaceZ[0] = msgSizeZ;

	offsetFaceX[0] = 0;
	offsetFaceY[0] = 0;
	offsetFaceZ[0] = 0;

	for (int i = 1; i < nMessages; i++)
	{
		countFaceX[i] = msgSizeX;
		countFaceY[i] = msgSizeY;
		countFaceZ[i] = msgSizeZ;

		offsetFaceX[i] = offsetFaceX[i-1] + msgSizeX;
		offsetFaceY[i] = offsetFaceY[i-1] + msgSizeY;
		offsetFaceZ[i] = offsetFaceZ[i-1] + msgSizeZ;
	}

	countFaceX[nMessages-1] = bufferSizeX - offsetFaceX[nMessages-1];
	countFaceY[nMessages-1] = bufferSizeY - offsetFaceY[nMessages-1];
	countFaceZ[nMessages-1] = bufferSizeZ - offsetFaceZ[nMessages-1];

	 if (myRank == 0)  printf("Message Size (%d, %d, %d)\n", msgSizeX*sizeof(double), msgSizeY*sizeof(double), msgSizeZ*sizeof(double));

	for (int localRankId = 0; localRankId < localRankCount; localRankId++)
	{
		Up[localRankId].SendBuffer    = (double**) calloc (sizeof(double*),gDepth);
		Down[localRankId].SendBuffer  = (double**) calloc (sizeof(double*),gDepth);
		East[localRankId].SendBuffer  = (double**) calloc (sizeof(double*),gDepth);
		West[localRankId].SendBuffer  = (double**) calloc (sizeof(double*),gDepth);
		North[localRankId].SendBuffer = (double**) calloc (sizeof(double*),gDepth);
		South[localRankId].SendBuffer = (double**) calloc (sizeof(double*),gDepth);
		Up[localRankId].RecvBuffer    = (double**) calloc (sizeof(double*),gDepth);
		Down[localRankId].RecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
		East[localRankId].RecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
		West[localRankId].RecvBuffer  = (double**) calloc (sizeof(double*),gDepth);
		North[localRankId].RecvBuffer = (double**) calloc (sizeof(double*),gDepth);
		South[localRankId].RecvBuffer = (double**) calloc (sizeof(double*),gDepth);

		for (int i = 0; i < gDepth; i++)
		{
			Up[localRankId].SendBuffer[i]    = (double*) calloc (sizeof(double),bufferSizeZ);
			Down[localRankId].SendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeZ);
			East[localRankId].SendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
			West[localRankId].SendBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
			North[localRankId].SendBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
			South[localRankId].SendBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
			Up[localRankId].RecvBuffer[i]    = (double*) calloc (sizeof(double),bufferSizeZ);
			Down[localRankId].RecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeZ);
			East[localRankId].RecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
			West[localRankId].RecvBuffer[i]  = (double*) calloc (sizeof(double),bufferSizeX);
			North[localRankId].RecvBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
			South[localRankId].RecvBuffer[i] = (double*) calloc (sizeof(double),bufferSizeY);
		}
	}


	MPI_Request** request;
  int* request_count;

	request  = (MPI_Request**) calloc (sizeof(MPI_Request*), localRankCount);
	for (int localRankId = 0; localRankId < localRankCount; localRankId++) request[localRankId] = (MPI_Request*) calloc (sizeof(MPI_Request), 24*gDepth*nMessages);
	request_count = (int*) calloc (sizeof(int), localRankCount);

  double computeTime = 0;
  double packTime = 0;
  double unpackTime = 0;
  double sendTime = 0;
  double recvTime = 0;
  double waitTime = 0;
  double scheduleTime = 0;
	double t_init;
	double t_end;

  int pos = 0;

  MPI_Barrier(MPI_COMM_WORLD);
  double execTime = -MPI_Wtime();

	for (int iter=0; iter<nIters; iter++)
  {
		for (int localRankId = 0; localRankId < localRankCount; localRankId++)
		{
			if (useTiming) t_init = MPI_Wtime();

			request_count[localRankId] =0;
			for (int k0 = start[localRankId].z; k0 < end[localRankId].z; k0+=BLOCKZ) {
				int k1= k0+BLOCKZ<end[localRankId].z?k0+BLOCKZ:end[localRankId].z;
				for (int j0 = start[localRankId].y; j0 < end[localRankId].y; j0+=BLOCKY) {
					int j1= j0+BLOCKY<end[localRankId].y?j0+BLOCKY:end[localRankId].y;
					for (int k = k0; k < k1; k++) {
						 for (int j = j0; j < j1; j++){
								#pragma vector aligned
								#pragma ivdep
								for (int i = start[localRankId].x; i < end[localRankId].x; i++)
								{
									double sum = 0;
									sum += U[fx*fy*k     + fx*j         + i] * stencil_coeff[0]; // Central
									for (int d = 1; d <= gDepth; d++)
									{
										double partial_sum = 0;
										partial_sum += U[fx*fy*(k-d) + fx*j         + i]; // Up[localRankId]
										partial_sum += U[fx*fy*(k+d) + fx*j         + i]; // Down[localRankId].
										partial_sum += U[fx*fy*k     + fx*j     - d + i]; // East[localRankId].
										partial_sum += U[fx*fy*k     + fx*j     + d + i]; // West[localRankId].
										partial_sum += U[fx*fy*k     + fx*(j+d)     + i]; // North[localRankId]
										partial_sum += U[fx*fy*k     + fx*(j-d)     + i]; // South[localRankId]
										sum += partial_sum * stencil_coeff[d];
									}
									Un[fx*fy*k     + fx*j + i] = sum; // Up[localRankId]date
								}
						 }
					}
				}
			}
			double *temp = NULL;temp = U;	U = Un;	Un = temp;

			if (useTiming) { t_end = MPI_Wtime();	computeTime += double(t_end-t_init);	}

			 if (useTiming) t_init = MPI_Wtime();

			 if(Down[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(Down[localRankId].RecvBuffer[d]  + offsetFaceZ[i],  count_down[localRankId]*countFaceZ[i],  MPI_DOUBLE, Down[localRankId].globalProcessId,  Up[localRankId].tag,    MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(Up[localRankId].type    == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(Up[localRankId].RecvBuffer[d]    + offsetFaceZ[i],    count_up[localRankId]*countFaceZ[i],    MPI_DOUBLE, Up[localRankId].globalProcessId,    Down[localRankId].tag,  MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(East[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(East[localRankId].RecvBuffer[d]  + offsetFaceX[i],  count_east[localRankId]*countFaceX[i],  MPI_DOUBLE, East[localRankId].globalProcessId,  West[localRankId].tag,  MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(West[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(West[localRankId].RecvBuffer[d]  + offsetFaceX[i],  count_west[localRankId]*countFaceX[i],  MPI_DOUBLE, West[localRankId].globalProcessId,  East[localRankId].tag,  MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(North[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(North[localRankId].RecvBuffer[d] + offsetFaceY[i], count_north[localRankId]*countFaceY[i], MPI_DOUBLE, North[localRankId].globalProcessId, South[localRankId].tag, MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(South[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Irecv(South[localRankId].RecvBuffer[d] + offsetFaceY[i], count_south[localRankId]*countFaceY[i], MPI_DOUBLE, South[localRankId].globalProcessId, North[localRankId].tag, MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);

			 if (useTiming)	 { t_end = MPI_Wtime();	 recvTime += double(t_end-t_init);	 }

				if (useTiming) t_init = MPI_Wtime();

			 if(Down[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*(end[localRankId].z-gDepth+d) + fx*start[localRankId].y            + start[localRankId].x              ],   packCount*count_down[localRankId],  faceZ_type,  Down[localRankId].SendBuffer[d],    sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
			 if(Up[localRankId].type    == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*(start[localRankId].z+d)      + fx*start[localRankId].y            + start[localRankId].x              ],   packCount*count_up[localRankId],    faceZ_type,  Up[localRankId].SendBuffer[d],  sizeof(double)*size.y*size.x, &pos, MPI_COMM_WORLD); pos = 0;}
			 if(East[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start[localRankId].z          + fx*start[localRankId].y            + (end[localRankId].x-gDepth+d)     ],   packCount*count_east[localRankId],  faceXX_type, East[localRankId].SendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
			 if(West[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start[localRankId].z          + fx*start[localRankId].y            + (start[localRankId].x+d)          ],   packCount*count_west[localRankId],  faceXX_type, West[localRankId].SendBuffer[d],  sizeof(double)*size.y*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
			 if(North[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start[localRankId].z          + fx*(start[localRankId].y+d)        + start[localRankId].x              ],   packCount*count_north[localRankId], faceY_type,  North[localRankId].SendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}
			 if(South[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Pack(&U[fx*fy*start[localRankId].z          + fx*(end[localRankId].y-gDepth+d)   + start[localRankId].x              ],   packCount*count_south[localRankId], faceY_type,  South[localRankId].SendBuffer[d], sizeof(double)*size.x*size.z, &pos, MPI_COMM_WORLD); pos = 0;}

			 if (useTiming) { t_end = MPI_Wtime(); packTime += double(t_end-t_init); }

				if (useTiming) t_init = MPI_Wtime();

			 if(Down[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(Down[localRankId].SendBuffer[d]  + offsetFaceZ[i],   count_down[localRankId]*countFaceZ[i],  MPI_DOUBLE,  Down[localRankId].globalProcessId,  Down[localRankId].tag,  MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(Up[localRankId].type    == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(Up[localRankId].SendBuffer[d]    + offsetFaceZ[i],     count_up[localRankId]*countFaceZ[i],    MPI_DOUBLE,  Up[localRankId].globalProcessId,    Up[localRankId].tag,    MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(East[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(East[localRankId].SendBuffer[d]  + offsetFaceX[i],   count_east[localRankId]*countFaceX[i],  MPI_DOUBLE,  East[localRankId].globalProcessId,  East[localRankId].tag,  MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(West[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(West[localRankId].SendBuffer[d]  + offsetFaceX[i],   count_west[localRankId]*countFaceX[i],  MPI_DOUBLE,  West[localRankId].globalProcessId,  West[localRankId].tag,  MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(North[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(North[localRankId].SendBuffer[d] + offsetFaceY[i],  count_north[localRankId]*countFaceY[i], MPI_DOUBLE,  North[localRankId].globalProcessId, North[localRankId].tag, MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);
			 if(South[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) for (int i = 0; i < nMessages; i++) MPI_Isend(South[localRankId].SendBuffer[d] + offsetFaceY[i],  count_south[localRankId]*countFaceY[i], MPI_DOUBLE,  South[localRankId].globalProcessId, South[localRankId].tag, MPI_COMM_WORLD, &request[localRankId][request_count[localRankId]++]);

			 if (useTiming)	 { t_end = MPI_Wtime();	 sendTime += double(t_end-t_init); }

				*temp = NULL;	temp = U;	U = Un;	Un = temp;
		}

		double *temp = NULL; temp = U; U = Un;	Un = temp;

	 for (int localRankId = 0; localRankId < localRankCount; localRankId++)
	 {
		 if (useTiming) t_init = MPI_Wtime();
		 MPI_Waitall(request_count[localRankId], request[localRankId], MPI_STATUS_IGNORE);
		 if (useTiming)	 { t_end = MPI_Wtime();	 waitTime += double(t_end-t_init); }
	 }


	 for (int localRankId = 0; localRankId < localRankCount; localRankId++)
	  {
			 if (useTiming) t_init = MPI_Wtime();

			 if(Down[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(Down[localRankId].RecvBuffer[d],  packCount*count_down[localRankId]*sizeof(double)*size.y*size.x,  &pos, &U[fx*fy*(end[localRankId].z+d)   + fx*start[localRankId].y     + start[localRankId].x    ],   packCount*count_down[localRankId],  faceZ_type, MPI_COMM_WORLD); pos = 0;}
			 if(Up[localRankId].type    == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(Up[localRankId].RecvBuffer[d],    packCount*count_up[localRankId]*sizeof(double)*size.y*size.x,    &pos, &U[fx*fy*d               + fx*start[localRankId].y     + start[localRankId].x    ],   packCount*count_up[localRankId],    faceZ_type,  MPI_COMM_WORLD); pos = 0;}
			 if(East[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(East[localRankId].RecvBuffer[d],  packCount*count_east[localRankId]*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start[localRankId].z     + fx*start[localRankId].y     + end[localRankId].x + d  ],   packCount*count_east[localRankId],  faceXX_type, MPI_COMM_WORLD); pos = 0;}
			 if(West[localRankId].type  == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(West[localRankId].RecvBuffer[d],  packCount*count_west[localRankId]*sizeof(double)*size.y*size.z,  &pos, &U[fx*fy*start[localRankId].z     + fx*start[localRankId].y     + d          ],   packCount*count_west[localRankId],  faceXX_type, MPI_COMM_WORLD); pos = 0;}
			 if(North[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(North[localRankId].RecvBuffer[d], packCount*count_north[localRankId]*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start[localRankId].z   + fx*d           + start[localRankId].x    ],   packCount*count_north[localRankId], faceY_type, MPI_COMM_WORLD); pos = 0;}
			 if(South[localRankId].type == REMOTE) for (int d = 0; d < gDepth; d++) { MPI_Unpack(South[localRankId].RecvBuffer[d], packCount*count_south[localRankId]*sizeof(double)*size.x*size.z, &pos, &U[fx*fy*start[localRankId].z   + fx*(end[localRankId].y+d)   + start[localRankId].x    ],   packCount*count_south[localRankId], faceY_type, MPI_COMM_WORLD); pos = 0;}

			 if (useTiming)	 { t_end = MPI_Wtime();	 unpackTime += double(t_end-t_init); }
		}
  }

	MPI_Barrier(MPI_COMM_WORLD);
	execTime += MPI_Wtime();

  double res = 0;
  double err = 0;
  for (int k=gDepth; k<gDepth+nz; k++)
	for (int j=gDepth; j<gDepth+ny; j++)
	for (int i=gDepth; i<gDepth+nx; i++)
  { double r = U[k*fy*fx + j*fx + i];  err += r * r; }
  MPI_Reduce (&err, &res, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  res = sqrt(res/((double)(N-1)*(double)(N-1)*(double)(N-1)));

  double meanComputeTime = 0, sumComputeTime = 0;
  double meanPackTime = 0, sumPackTime = 0;
  double meanUnpackTime = 0, sumUnpackTime = 0;
  double meanSendTime = 0, sumSendTime = 0;
  double meanRecvTime = 0, sumRecvTime = 0;
  double meanWaitTime = 0, sumWaitTime = 0;

  MPI_Reduce(&computeTime, &sumComputeTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&packTime, &sumPackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&unpackTime, &sumUnpackTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&sendTime, &sumSendTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&recvTime, &sumRecvTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&waitTime, &sumWaitTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  #if USE_PAPI == 1
  MPI_Reduce(&computeMisses, &computeMissesSum, PAPI_EVENTS, MPI_LONG_LONG_INT, MPI_SUM, 0, MPI_COMM_WORLD);
  #endif

  meanComputeTime = sumComputeTime / globalRankCount;
  meanPackTime = sumPackTime / globalRankCount;
  meanUnpackTime = sumUnpackTime / globalRankCount;
  meanSendTime = sumSendTime / globalRankCount;
  meanRecvTime = sumRecvTime / globalRankCount;
  meanWaitTime = sumWaitTime / globalRankCount;

  double totalTime = meanComputeTime + meanRecvTime + meanPackTime + meanSendTime + meanWaitTime + meanUnpackTime;

  if (useTiming == false) totalTime = execTime;

	if(myRank ==0) {
	   double gflops = nIters*(double)N*(double)N*(double)N*(2 + gDepth * 8)/(1.0e9);
	   printf("Compute:     %.4f\n", meanComputeTime);
	   printf("MPI_Irecv:   %.4f\n", meanRecvTime);
	   printf("MPI_Isend:   %.4f\n", meanSendTime);
	   printf("MPI_Pack:    %.4f\n", meanPackTime);
	   printf("MPI_Unpack:  %.4f\n", meanUnpackTime);
	   printf("MPI_Waitall: %.4f\n", meanWaitTime);
     #if USE_PAPI == 1
		 printf("L2 Misses: %ld, L3 Misses: %ld\n", computeMissesSum[0], computeMissesSum[1]);
	   #endif
	   printf("%.4fs, %.3f GFlop/s (Error: %.4f); (L2 Error: %g)\n", totalTime, gflops/totalTime, execTime-totalTime, res);
	}

    MPI_Finalize(); return 0;
}
