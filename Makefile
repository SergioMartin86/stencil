## Add the following line in the UPC++ application Makefile
#include $(UPCXX_INSTALL_DIR)/include/upcxx.mak
GHOST_DEPTH=2

ifeq ($(vtune),1)
INCLUDE_VTUNE    = -I/opt/intel/vtune_amplifier_xe_2017.2.0.499904/include
LIBDIR_VTUNE     = -L/opt/intel/vtune_amplifier_xe_2017.2.0.499904/lib64
LIBS_VTUNE       = -littnotify
CCFLAGS_VTUNE    = -D__VTUNE
endif

ifeq ($(tcmalloc),1)
LIBS_TCMALLOC     = -ltcmalloc -fno-builtin-malloc -fno-builtin-calloc
LIBDIR_TCMALLOC   = -L/global/homes/s/sergiom/lib/tcmalloc/lib/
endif

CFLAGS = -O3 -Wfatal-errors $(INCLUDE_VTUNE)  #-std=c++11  #-qopt-report=5

#AMPIDIR = ~/src/charmgit
AMPIDIR = ~/src/charm-v6.8.2

ifeq ($(MATE_DEBUG),1)
        CFLAGS = -O0 -g -Wfatal-errors -I/global/homes/s/sergiom/libs/fpmpi/include -DFPMPI_ENABLE -L/global/homes/s/sergiom/libs/fpmpi/lib -lfpmpi $(LIBS_VTUNE) $(CCFLAGS_VTUNE) $(LIBDIR_VTUNE) $(INCLUDE_VTUNE)
endif
CC = CC

.SECONDARY: 
BINARIES = stencil_MPI
#BINARIES = stencil_UPC
BINARIES += stencil_MPIOLAP
#BINARIES += stencil_TOUCAN
BINARIES += stencil_HYBRID
#BINARIES += stencil_MATE_NOHYBRID
#BINARIES += stencil_UPCMATE
BINARIES += stencil_OMP

.PHONY: all stage
all: $(BINARIES)
stage: $(BINARIES)
	cp -f $(BINARIES) jobscripts

stencil_MPI: stencil_MPI.o
	$(CC) $(CFLAGS) -o $@ $^

stencil_UPC: stencil_UPC.o
	upcxx $(CFLAGS) -o $@ $^

stencil_MPIOLAP: stencil_MPIOLAP.o
	$(CC) $(CFLAGS) -o $@ $^

stencil_OMP: stencil_OMP.o
	$(CC) -o $@ $^ $(CFLAGS) -fopenmp -pthread

stencil_HYBRID: mate_stencil_HYBRID.o
	mate-cxx -o $@ $^ $(CFLAGS)

stencil_TOUCAN: mate_stencil_TOUCAN.o
	mate-cxx -o $@ $^ $(CFLAGS)

stencil_UPCMATE: mate_stencil_UPCMATE.o
	mate-cxx -o $@ $^ $(CFLAGS)

stencil_MATE_NOHYBRID: mate_stencil_MATE_NOHYBRID.o
	mate-cxx -o $@ $^ $(CFLAGS)

# Automatic handling of rules below here

mate_%.o: mate_%.cpp
	mate-cxx -c $(CFLAGS) $<

mate_%.cpp: %.cpp
	mate-translate $< 

stencil_UPC.o: stencil_UPC.cpp
	upcxx -o $@ -c $(CFLAGS) $< 

stencil_UPCMATE.o: stencil_UPCMATE.cpp
	mate-cxx -o $@ -c $(CFLAGS) $< 

%.o: %.cpp
	$(CC) -c $(CFLAGS) $< -Wno-unknown-pragmas

.PHONY: clean
clean:
	$(RM) $(BINARIES) *.o mate_*.cpp *.ti *.optrpt *.dat


	
